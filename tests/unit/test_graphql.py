import json

import pydgraph

from daggerpy import graphql


class MockDgraphClient:
    def __init__(self, *args, **kwargs):
        self.mock_response = kwargs.pop('response')

    def txn(self, *args, **kwargs):
        return MockDgraphTxn(response=self.mock_response)


class MockDgraphTxn:
    def __init__(self, *args, **kwargs):
        self.mock_response = kwargs.pop('response')

    def query(self, *args, **kwargs):
        return MockDgraphResponse(response=self.mock_response)


class MockDgraphResponse:
    def __init__(self, *args, **kwargs):
        self.json = json.dumps(kwargs.pop('response'))


def test_query_worker_results(monkeypatch):
    '''Ensure all result entries are returned

    Breaking this contract will lead to false cache misses!
    '''

    result = {
        "worker": [
            {
                "results": [
                    {
                        "uid": "0x24ddb",
                        "exitcode": 137
                    },
                    {
                        "uid": "0x2793b",
                        "exitcode": 0
                    }
                ]
            }
        ]
    }

    def mock_dgraph_response(*args, **kwargs):
        return MockDgraphClient(response=result)

    monkeypatch.setattr(pydgraph, 'DgraphClient', mock_dgraph_response)
    client = graphql.create_client('localhost:8090')
    response = graphql.query_worker_results(client, 'localhost', '0xaaa')
    assert response == result['worker'][0]['results']
