import json

from time import sleep

from daggerpy import graphql
from daggerpy import client

from daggerpy.settings import GRAPHQL_SERVER, DATADIR


def processor():
    cmd = client.Concat('cmd')(
        'echo -n ',
        client.Literal('cmd'),
        ' > out.txt')

    p = client.Docker(
        'arg2stdout',
        image='alpine:3.10',
        entrypoint='ash',
        cpus=0.2,
        mem_mb=8
    )('-c', cmd)

    return client.Input(p, monad='maybe')


def test_maybe():
    return client.Map('test-maybe-edge')(
        processor(),
        client.List('cmd-list')('foobarbaz', 'barbazfoo')
    )


def _run_to_completion(client):
    while True:
        query_result = graphql.query_active_jobs(client, 'localhost')

        if all(n.get('state') == 'done' for n in query_result):
            break

        sleep(1)


def run():
    g = test_maybe()

    if g.test(publish=True):
        jid = g.bind()

        print(f'waiting for job {jid} to complete ...')
        c = graphql.create_client(GRAPHQL_SERVER)
        _run_to_completion(c)

        return jid


if __name__ == '__main__':
    jid1 = run()

    with open(DATADIR / f'{jid1}.json') as f:
        result = json.load(f)

    print(f'result = {result}')

    file_obj = result[0]['files'][0]

    missing_file = DATADIR / f'{file_obj["crc"]}-{file_obj["filename"]}'
    print(f'Removing file: {missing_file}')
    missing_file.unlink()
    print('Rerun job, expect one cache hit and one rerun for missing file')
    run()
    print('Rerun job, expect all cache hits')
    run()

    assert missing_file.exists()
