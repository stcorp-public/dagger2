# This chain is exposing a bug in dagger, where the export processor looses its
# input to the clearsky ref after the clearsky ref is used for mapping over maja
# processor!

from time import sleep
from uuid import uuid4

from daggerpy import client as dc
from daggerpy import graphql
from daggerpy.evaluate import result_resolver


GRAPHQL_SERVER = 'localhost:9080'
LCDB_EXPORT_CONTAINER_NAME = str(uuid4())


def _run_to_completion(client):
    while True:
        query_result = graphql.query_active_jobs(client, 'localhost')

        if all(n.get('state') == 'done' for n in query_result):
            break

        sleep(1)


def clearskyref(ref):
    unsorted_l1c_clearsky = dc.Docker(
        's2gtiff-clearsky',
        image='dagger-test-gen3from1input:test',
        cpus=0.2,
        mem_mb=512
    )(ref, 's2gtiff-clearsky')

    return dc.Node('sort-clearskyref', fn='filesort')(
        unsorted_l1c_clearsky.get('files')
    )


def maja(l1c_clearsky_60m):
    maja = dc.Docker(
        'maja',
        image='dagger-test-gen3from2input:test',
        cpus=0.2,
        mem_mb=512
    )(l1c_clearsky_60m,
      dc.Node('target-60m').get('2'),
      'maja')
    return maja.get('files/0')


def sntnel_l1c():
    s2gtiff = dc.Docker(
        's2gtiff',
        image='dagger-test-gen3from1input:test',
        mem_mb=512,
        cpus=1
    )(dc.Literal('s2id'), 's2gtiff')
    sorted_gtiffs = dc.Node('gtif-sort', fn='filesort')(
        dc.Input(s2gtiff.get('files'))
    )

    return dc.Input(sorted_gtiffs, monad='maybe')


def preprocess_timeserie(params):
    l1c_clearsky = clearskyref(params.get('clearskyref'))
    lcdb_cls = dc.Docker(
        LCDB_EXPORT_CONTAINER_NAME,
        image='alpine:3.10',
        mem_mb=512,
        cpus=1,
        entrypoint='cat',
    )(
        l1c_clearsky.get('0')
    )

    l1c_tileserie = dc.Map('tile-serie')(
        sntnel_l1c(),
        dc.List('maja-list')('a')
    )

    cloudmasks = dc.Map('cloudmasks')(
        maja(l1c_clearsky.get('2')),
        l1c_tileserie
    )

    return dc.Dict('outputs')(
        ('lcdb-cls', lcdb_cls),
        ('cloudmasks', cloudmasks)
    )


def run_training_data():
    return preprocess_timeserie(dc.Dict('melhus-inputs')(
        ('clearskyref', 'foo')
    ))


if __name__ == '__main__':
    g = run_training_data()
    # g.visualize()

    if g.test(publish=True):
        jid = g.bind()
        client = graphql.create_client(GRAPHQL_SERVER)
        _run_to_completion(client)

        docker_nodes = graphql.query_nodes_by_name(client,
                                                   LCDB_EXPORT_CONTAINER_NAME)
        docker_node, = (n for n in docker_nodes['nodes'] if 'state' in n)
        uid = docker_node['uid']
        print(docker_node)
        result = result_resolver(client, uid)(uid)
        print(result)
