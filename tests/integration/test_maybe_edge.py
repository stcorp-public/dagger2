from daggerpy import client

from tests.integration.common import get_result


def processor():
    p = client.Docker(
        'arg2stdout',
        image='python:3.7.4-alpine3.10',
        entrypoint='python',
        cpus=0.2,
        mem_mb=8
    )('-c', "import sys; i = int(sys.argv[-1].strip()); print(i, end='')",
      client.Literal('cmd'))

    return client.Input(p, monad='maybe')


def test_maybe():
    return client.Map('test-maybe-edge')(
        processor(),
        client.List('cmd-list')(
            '1',
            '2',
            'p',    # <- will fail, but should not cause job failure!
            '4',
        ))


if __name__ == '__main__':
    g = test_maybe()

    if g.test(publish=True):
        result = get_result(g.bind())

        print(f'result = {result}')
        assert set(int(r['stdout']) for r in result) == {1, 2, 4}
