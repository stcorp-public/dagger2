from daggerpy import client, graphql, state
from daggerpy.settings import GRAPHQL_SERVER

from tests.integration.common import run_to_completion


def processor():
    return client.Docker(
        'test-oom',
        image='python:3.7.4-alpine3.10',
        entrypoint='python',
        cpus=0.2,
        mem_mb=8
    )('-c', client.Literal('cmd'))


def test_oom():
    return client.Map('test-oom-failure-state')(
        processor(),
        client.List('cmd-list')(
            'foo = 8_000_000_000 * chr(90)',
        ))


if __name__ == '__main__':
    g = test_oom()

    if g.test(publish=True):
        jobuid = g.bind()

        dgraph_client = graphql.create_client(GRAPHQL_SERVER)
        run_to_completion(dgraph_client, jobuid, desired_state=state.ABORTED)
        failure = graphql.query_job_failure(dgraph_client, jobuid)

        print(f'failure = {failure}')
        assert failure['state'] == state.OOMKILLED
    else:
        assert False
