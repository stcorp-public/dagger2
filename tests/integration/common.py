from time import sleep

from daggerpy import graphql, state

from daggerpy.evaluate import result_resolver
from daggerpy.settings import GRAPHQL_SERVER


def run_to_completion(client, job_uid, desired_state=state.DONE):
    while True:
        query_result = graphql.query_all_jobs(client, 'localhost')
        print(f'Reached state: {query_result}')

        if all(n.get('state') == desired_state for n in query_result
               if n['uid'] == job_uid):
            break

        sleep(0.1)


def get_result(jid, desired_state=state.DONE):
    print(f'waiting for job {jid} to complete ...')
    c = graphql.create_client(GRAPHQL_SERVER)
    run_to_completion(c, jid, desired_state)

    return result_resolver(c, jid)(jid)
