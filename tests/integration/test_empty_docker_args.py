from daggerpy import client

from tests.integration.common import get_result


def echo():
    return client.Docker(
        'echo',
        image='alpine:3.10',
        cpus=0.2,
        mem_mb=8
    )('echo', '-n', 'foo', client.Literal('empty-arg'))


def test_empty_arg():
    return client.Map('test-empty-docker-args')(
        echo(),
        client.List('args')(
            'bar',  # <- yeilds stdout: `foo bar`
            '',     # <- yeilds stdout: `foo`, ignoring empty string
        ))


if __name__ == '__main__':
    g = test_empty_arg()
    # Empty string ignored, {stdout: "foo\n"}

    if g.test(publish=True):
        result = get_result(g.bind())
