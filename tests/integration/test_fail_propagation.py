from daggerpy import client, graphql, state
from daggerpy.settings import GRAPHQL_SERVER

from tests.integration.common import get_result


def processor():
    return client.Docker(
        'arg2stdout',
        image='python:3.7.4-alpine3.10',
        entrypoint='python',
        cpus=0.2,
        mem_mb=8
    )('-c', "import sys; i = int(sys.argv[-1].strip()); print(i, end='')",
      client.Literal('cmd'))


def test_maybe():
    return client.Map('test-failing-job')(
        processor(),
        client.List('cmd-list')(
            '1',
            '2',
            'p',    # <- will cause job fail!
            '4',
        ))


def main():
    g = test_maybe()

    if g.test(publish=True):
        jobuid = g.bind()
        result = get_result(jobuid, desired_state=state.ABORTED)
        print(f'result = {result}')
        assert result[2]['stderr'] != ''
        assert result[2]['stdout'] == ''

        dgraph_client = graphql.create_client(GRAPHQL_SERVER)
        job_node = graphql.get_full_node(dgraph_client, jobuid)

        print(f'job_node = {job_node}')
        assert job_node['state'] == state.ABORTED
    else:
        assert False


if __name__ == '__main__':
    main()
    main()
