#!/usr/bin/env bash

set -e


# dgraph only accepts admin REST requests from whitelisted IP's, hence the added
# complexity of explicit docker networks and IP address fetching.

RUNNING_DGRAPH_CONTAINERS=$(docker-compose ps \
		--services \
		--filter "status=running" \
		2>/dev/null \
	| wc -l)

if [ "$RUNNING_DGRAPH_CONTAINERS" -ne 3 ]; then
	docker network create dgraph_dev &> /dev/null || true
	WHITELISTED=$(docker network inspect dgraph_dev | jq '.[0].IPAM.Config[0].Gateway')
	export WHITELISTED
	docker-compose up -d
fi

echo Waiting for dgraph to get online ...
while curl -s localhost:8080/state | jq -e '.errors?' > /dev/null; do
	sleep 1
done

if [ "$DAGGER_MODE" = 'iknowwhatimdoing' ]; then
	echo Dropping localhost db..
else
	read -r -p "Drop all in localhost DB... (press enter)"
fi
curl localhost:8080/alter \
  -H "X-Dgraph-CommitNow: true" \
  -XPOST \
  -d '{"drop_all": true}' 2>/dev/null | python3 -m json.tool

echo "Set schema in localhost DB..."
curl localhost:8080/alter \
  -H "X-Dgraph-CommitNow: true" \
  -XPOST \
  -d "$(cat schema)" 2>/dev/null | python3 -m json.tool


echo "Register worker hostname"
dagger worker register localhost --server localhost
