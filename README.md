# Dagger 2

Detailed information and a tutorial is found in the [documentation](https://stcorp-public.gitlab.io/dagger2).

Concept:

  - Functional, graph-based processing system.
  - UNIX-friendly graph compositions of dockerized command-line programs.
  - Simple data model -- observe and visualize state directly through GraphQL.
  - Vertex-centric and flow-based; every node is a function.
  - The `input` predicate models data dependencies.
  - Typed: (`file`, `literal`, ...)
  - Synchronous graph type evaluation (easy for client to verify).
  - Asynchronous graph process evaluation -- long-running, mutates, and updated
    frequently. Client polls state and displays literal data and file URLs.
  - Manages data repository, file tree indexed directly by graph.
  - Async graph evals are bound to VM hostname.

The idea as shortly as possible is to model (big) computation by dynamic
graphs, offer caching/memoization, and index the complete data repository by
GraphQL.

See [dgraph docs](https://docs.dgraph.io/).


## Getting started

#### Prerequisite: Run dgraph

```bash
$ ./initdb.sh
```

#### Install the dagger client

```bash
$ python3 -m venv venv
$ source venv/bin/activate
(venv)$ pip install wheel
(venv)$ pip install -r requirements.in/requirements-dev.txt
(venv)$ pip install -e .
```

#### Set local development config

```bash
$ cat > .env << EOL
GRAPHQL_SERVER=localhost:9080
GRAPHQL_HTTP_SERVER=localhost:8080
DAGGER_WORKER=localhost
DAGGER_DATADIR=./data
EOL
```

**NOTE**: The `dagger` command is only available when the virtualenv is activated.

```bash
$ dagger worker register localhost
```

#### Start a worker with two concurrent processes and 4GB memory

```bash
$ dagger worker run -n 2 -m 4096
```

#### Run an example

```bash
$ python3 examples/quicklook.py
```

The client will show the job id, or you can fetch it from `dagger job history`.

When the job has completed, you can view the result object from the dagger datadir, named `<JOB-ID>.json`.
E.g, if the job id is `<0xbeef>`, you can view the result object using `jq`:

```bash
$ jq -r '.[].info' ./data/0xbeef.json
```

#### View job queue

```bash
$ dagger job queue
```

#### View job history

List all jobs with status and Job node ID's (JID)

```bash
$ dagger job history
```

#### List all docker nodes for a JID

JID found in job queue/history

```bash
$ dagger job processors <JID>
```

#### Search for nodes

Search by name:

```bash
$ dagger node search -n "quicklook"
```

#### Get nodes by UID

```bash
$ dagger node get UID
```

#### Inspect output of docker nodes

Use `jq` to print outputs:

```bash
$ dagger node get UID | jq -r '.result[].stdout'
```

#### Get docker run command for manual debugging

Requires correctly configured `DAGGER_DATADIR` to resolve files.

```bash
$ dagger job docker JID
```

#### More

For complete CLI usage, see:

```bash
$ dagger --help
```

## Python client library

Implements a [Keras-like API](https://keras.io/) for building graphs.

## Recommended tools

  * [ctop](https://github.com/bcicen/ctop)
  * `docker` CLI tools, e.g. `docker logs`, `docker inspect`, etc.
  * [jq](https://stedolan.github.io/jq/)


## GraphQL GUI (Ratel)

Browse to `localhost:8000` for a GraphQL web interface (shipped with dgraph).
Example queries:

```graphql
{
  docker_nodes(func: eq(fn, "docker")) @filter(has(state)) {
    fn
    name
    doc
    input {
      uid
      name
      literal
    }
  }
}
```

```
{
  worker(func: eq(hostname, "localhost")) {
    uid
    expand(_all_) {
      expand(_all_) {
        expand(_all_) {
          expand(_all_)
        }
      }
    }
  }
}
```

## Schema

The dgraph schema definition is stored in [the schema file](schema)
