# S[&]T Norway Python setup.py
# ============================
#
# * Versions are set with annotated git tags (`git tag -a`).
# * Python module version is set in `mypackage.__version__`.
# * Automatic version if no git tag is set.
#
# If the commit doesn't have an annotated tag, the version will
# automatically receive a suffix that includes the commit SHA. E.g:
# `2.1.4+git40674da708fa3771719d066005be84e83734ce8d`. This is useful for
# automated testing.
#
# Workflow
# ========
#
# When code is merged onto a mainline branch, create an annotated tag with an
# empty message and semver WITHOUT prefix. E.g: `git tag -a -m '' 1.2.7`. Do not
# use a version prefix like: `v0.3.1`.
#
# Push the tags to the central repository (Gitlab). Automated deployment should
# be automatically managed by means of git tags.

from setuptools import setup


PACKAGE_NAME = 'daggerpy'


setup(
    name=PACKAGE_NAME,
    use_scm_version=True,
    description='Graph processing framework',
    url='https://stcorp.no',
    author='Michael Soukup',
    author_email='sysadmin@stcorp.no',
    license='All rights reserved',
    entry_points={
        'console_scripts': [
            f'dagger={PACKAGE_NAME}.cli:cli'
        ]
    },
    packages=[
        PACKAGE_NAME,
    ],
    install_requires=[
        'pydgraph>=2.0',
        'click',
        'requests',
        'python-dotenv',
        'toolz'
        # DO NOT use version pinning, e.g: 'click==7.0.1',
        # This means don't read from requirements.txt!
        # However min version is fine, e.g: 'click>=6',
        # As is max version, e.g: 'click<7'
        # Range is also OK, e.g: 'click<7,>=6'
    ],
    data_files=[
        ('dgraph', ['schema']),
        ('meta', ['requirements.txt']),
    ],
    setup_requires=['setuptools_scm'],
    include_package_data=True,
    zip_safe=False
)
