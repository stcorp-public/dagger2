"""Custom dagger types."""
from daggerpy import state


class Awaited(object):
    """Represents an awaited computation."""
    def __repr__(self):
        return '<awaited>'


class Aborted(object):
    """Represents an aborted computation."""
    def __repr__(self):
        return f'<{state.ABORTED}>'


class Failed(object):
    """Represents a failed computation."""
    def __repr__(self):
        return f'<{state.FAILED}>'


class Ordered(object):
    """Assign a positional order to an input edge."""
    def __init__(self, index, value):
        self.index = int(index)
        self.value = value

    def __repr__(self):
        return 'Ordered <%d: %s>' % (self.index, str(self.value))
