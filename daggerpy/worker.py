"""Polls and evaluates nodes bound to it's hostname.

This worker polls active jobs from dgraph and starts processing whenever
empty or 'start' state is set on a job.

Each worker is a separate graph indexing its data repository, where the root
hostname node holds state about the running system.
"""

import time
import json
import sys

from socket import getfqdn

from grpc import RpcError
from datetime import datetime

from daggerpy import graphql
from daggerpy import docker
from daggerpy import util
from daggerpy import state
from daggerpy.evaluate import node_evaluator
from daggerpy.util import sizeof_fmt, dir_size
from daggerpy.types import Awaited, Aborted, Failed
from daggerpy.settings import GRAPHQL_SERVER, DATADIR, TEST_MODE


def daemon(hostname, mem, max_running, cpus):
    fqdn = getfqdn()

    if not TEST_MODE and hostname != 'localhost' and hostname != fqdn:
        sys.exit(f'Invalid config! Specified hostname ({hostname}) '
                 f'does not match system FQDN ({fqdn})!')

    from daggerpy import __version__ as dagger_version

    print(f"Running dagger daemon '{dagger_version}' on: {hostname}")
    print("CPUs: %d" % cpus)
    print("Max memory: %d" % mem)
    print("Max concurrent containers: %d" % max_running)

    client = graphql.create_client(GRAPHQL_SERVER)

    try:
        worker_state = graphql.get_worker_state(client, hostname)
    except RpcError:
        sys.exit('\nERROR! Could not connect to dgraph! Is dgraph running?')

    util.mk_workdir(DATADIR)
    print("Connected to GraphQL server on %s" % GRAPHQL_SERVER)

    if worker_state is None:
        sys.exit('No dagger worker registered for hostname '
                 f'`{hostname}`. Run `dagger worker register`!')

    print('Cleaning up stale containers ...')
    stale_containers = docker.cleanup_stale_containers()

    if stale_containers:
        print('Cleaned up stale containers and data: '
              f'{" ".join(stale_containers)}')
    else:
        print('No stale containers found.')

    # Initial worker state
    # TODO Init state from $(docker stats)
    # TODO Verify and clean cache on startup
    worker_state = {
        'hostname': hostname,
        'last_seen': datetime.utcnow().isoformat(),
        'cpus': cpus,
        state.RUNNING: 0,
        'max_running': max_running,
        'mem_mb': 0,
        'max_mem_mb': mem,
        'disk_usage': dir_size(str(DATADIR))
    }

    graphql.update_worker_state(client, worker_state)

    # TODO verify files on disk -- loop through files bound to worker
    # and check for existence and crc.

    print(f"Data directory: {DATADIR}")
    print("Disk usage: %s" % sizeof_fmt(worker_state['disk_usage']))

    # Create a local cache
    cache = {}

    # Work forever.
    # Upon every iteration, update state and process active jobs.
    print("Ready to work")
    while True:
        # Get active jobs
        jobs = graphql.query_active_jobs(client, hostname)
        for job_node in jobs:
            job_state = job_node.get('state')
            job_name = job_node.get('name', '')

            if job_state is None:
                print(f"New job `{job_name}` <{job_node['uid']}>")
                job_node['started'] = datetime.utcnow().isoformat()
                graphql.set_node(client, job_node)
            elif job_state == state.KILLING:
                print(f'Force aborting (killing) job `{job_name}` '
                      f'<{job_node["uid"]}>')
                to_be_killed = []

                for container_name in docker.running_processors():
                    *_, job_id, _ = container_name.split('-')

                    if job_id == job_node['uid']:
                        to_be_killed.append(container_name)

                docker.kill(to_be_killed)
                print(f'Killed containers: {len(to_be_killed)}')
            elif job_state not in state.ONGOING:
                raise Exception("Unexpected state on node %s: `%s`"
                                % (job_node['uid'], job_state))

            evaluator = node_evaluator(hostname, client, cache, print,
                                       root=job_node['uid'])
            output = evaluator(job_node['uid'])
            # TODO: ensure job node state is set if evaluator fails to avoid new
            # job on same job uid.

            success = not any(isinstance(output, s)
                              for s in (Awaited, Aborted, Failed))

            if success:
                print(f"Job `{job_node.get('name', '')}` <{job_node['uid']}> "
                      f"completed with output: {json.dumps(output)}")

                with open(DATADIR / f'{job_node["uid"]}.json', 'w') as f:
                    f.write(json.dumps(output, indent=2))

            new_worker_state = graphql.get_worker_state(client, hostname)
            new_worker_state['last_seen'] = datetime.utcnow().isoformat()
            graphql.update_worker_state(client, new_worker_state)

            if job_state not in state.ONGOING:
                print("Containers running: %d" % new_worker_state['running'])
                print("Memory usage: %d Mb" % new_worker_state['mem_mb'])

        time.sleep(1)
