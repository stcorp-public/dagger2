"""Simple docker interface and data types."""

import os
import json
import shutil

from itertools import chain

from toolz import compose, curry
from toolz.curried import map, filter

from daggerpy import util
from daggerpy.primitives import file_sample, is_file
from daggerpy.settings import DATADIR


result_sample = {
    'files': [file_sample],
    'stdout': "",
    'stderr': ""
}

docker_kwargs = {
    'image': "",
    'workdir': "",
    'argv': [],
    'mem_mb': 256,
    'timeout': 60,
    'cpus': 1,
    'cache': 'yes',
    'stdin': "",
    'env': []
}


def sanitize_opts(**kwargs):
    """Sanitize the keyword arguments to the 'docker' node."""
    opts = {**docker_kwargs, **kwargs}
    diff = set(docker_kwargs.keys()).difference(set(opts.keys()))
    util.informed_assert(len(diff) == 0, "Unknown argument(s) %s" % diff)
    # Conditional arguments in a graph specified by daggerpy.client can
    # generate empty strings which must be omitted to avoid invalid subprocess
    # arg list containing elements of empty strings.
    opts['argv'] = [a for a in util.flatten(opts['argv']) if a != '']
    util.informed_assert(
        all(map(
            lambda arg: isinstance(arg, str) or is_file(arg),
            opts['argv']
        )),
        "Elements of argv must be strings or files"
    )
    return opts


def assert_image_opts(opts):
    """Assert docker image options."""
    # TODO call dockerhub and verify existence?
    # TODO more and better assertions
    util.informed_assert(
        bool(opts['image']),
        f'Invalid docker image: `{opts["image"]}`'
    )

    if opts['workdir']:
        util.informed_assert(
            os.path.isabs(opts['workdir']),
            f'Invalid workdir path: `{opts["workdir"]}`'
        )


def inspect(container_ids):
    """Inspect a set of containers."""
    if len(container_ids) == 0:
        return []
    return json.loads(util.check_stdout(['docker', 'inspect']
                                        + container_ids))


def inspect_state(container_name):
    """Inspect state of a single container."""
    cmd = (
        'docker',
        'inspect',
        '-f',
        '{{json .State}}',
        container_name
    )

    return json.loads(util.check_stdout(cmd))


def inspect_states(container_ids):
    """Inspect the state of a set of containers."""
    if len(container_ids) == 0:
        return []
    return compose(
        list,
        map(json.loads),
        lambda out: out.strip().split('\n'),
        util.check_stdout
    )(['docker', 'inspect', '-f', '{{json .State}}'] + container_ids)


def inspect_memory(container_ids):
    """Inspect the max memory of a set of containers."""
    if len(container_ids) == 0:
        return []
    return compose(
        list,
        map(int),
        lambda out: out.strip().split('\n'),
        util.check_stdout
    )(['docker', 'inspect', '-f', '{{.HostConfig.Memory}}'] + container_ids)


def ps(prefix='dagger-'):
    """Returns a list of dagger-container IDs."""
    return compose(
        list,
        filter(bool),
        lambda out: out.strip().split('\n'),
        util.check_stdout
    )(('docker', 'ps', '-aq', '-f', 'name=%s' % prefix))


def running_processors(prefix='dagger-'):
    """Returns a list of dagger-container names."""
    return compose(
        list,
        filter(bool),
        lambda out: out.strip().split('\n'),
        util.check_stdout
    )(('docker', 'ps', '-f', f'name={prefix}', '--format', '{{.Names}}'))


def kill(container_ids):
    '''Kill a list of container names/ids'''
    if len(container_ids) == 0:
        return

    util.subprocess.run(['docker', 'kill'] + container_ids)


def wipe(container_ids):
    '''Force remove list of container names/ids along with data volumes'''
    if len(container_ids) == 0:
        return

    util.subprocess.run(['docker', 'rm', '-f', '-v'] + container_ids)


def filediff(workdir, container_name):
    def trailing_slash(p):
        return p if p[-1] == '/' else p + '/'

    @curry
    def is_parent(child, parent):
        return os.path.commonpath([parent, child]) == child

    def remove_dirs(paths):
        '''Remove dir paths having common abspath with file paths'''
        return filter(
            lambda path: not any(map(
                is_parent(path),
                set(paths) - set([path])
            )),
            paths
        )

    cmd = ('docker', 'diff', container_name)

    return compose(
        list,
        sorted,
        map(lambda abspath: abspath[len(trailing_slash(workdir)):]),
        remove_dirs,
        list,
        filter(lambda abspath: abspath.startswith(workdir)),
        map(lambda line: line.split(' ')[1]),
        filter(lambda line: line.startswith('A')),
        lambda out: out.split('\n'),
        util.check_stdout
    )(cmd)


# args expected to be str or file object.
def resolve_args(input_dir, args):
    argv = []
    vmounts = []
    for arg in args:
        if isinstance(arg, str):
            argv.append(arg)
        else:
            file_path = os.path.join(input_dir, arg['filename'])
            relpath = DATADIR / f'{arg["crc"]}-{arg["filename"]}'
            vmounts.append((
                str(relpath),
                file_path,
                'ro'
            ))
            argv.append(file_path)
    return (argv, vmounts)


def docker_run_subprocess_args(container_name, opts, env, proc_dir):
    cpus = float(opts['cpus'])
    mem = str(int(opts['mem_mb']) * 1_048_576)     # * 1024^2
    workdir = opts['workdir'] or f'/{container_name}'
    container_input_dir = f'/{container_name}-input'
    # inputs files should not be mounted in workdir, when v-mounts are used!
    # This seems bad as files are mounted in to container, then placed back in
    # the v-mounted workdir.
    argv, input_vmounts = resolve_args(container_input_dir, opts['argv'])
    has_stdin = bool(opts['stdin'])
    cmd = list(chain(
        ('docker', 'run'),
        ('--init',),
        ('-i',) if has_stdin else ('-d',),
        ('--name', container_name),
        ('--cpus', str(cpus)),
        ('--stop-timeout', str(int(opts['timeout']) * 60)),
        ('--memory', mem),
        # Disable swap to keep memory usage honest
        ('--memory-swap', mem),
        # Unique workdir name avoids accidental overshadowing of image files
        ('--workdir', workdir),
        (('--entrypoint', opts['entrypoint'])
         if opts.get('entrypoint', False) else tuple()),
        ('-v', f'{proc_dir}:{workdir}'),
        chain.from_iterable(('-v', ':'.join(vmount))
                            for vmount in input_vmounts),
        chain.from_iterable(('-e', envvar) for envvar in env),
        (opts['image'],),
        argv
    ))
    stdin = opts['stdin'].encode() if has_stdin else None
    return cmd, stdin


def docker_run_args(opts, extra_opts=tuple()):
    '''Create interactive docker run commands'''
    cpus = float(opts['cpus'])
    mem = str(int(opts['mem_mb']) * 1_048_576)     # * 1024^2
    workdir = opts['workdir'] or '/test_workdir'
    argv, vmounts = resolve_args(
        workdir,
        opts['argv']
    )

    cmd = list(chain(
        ('docker', 'run'),
        ('-ti',),
        chain.from_iterable(extra_opts),
        ('--cpus', str(cpus)),
        ('--memory', mem),
        # Disable swap to keep memory usage honest
        ('--memory-swap', mem),
        ('--workdir', workdir),
        (('--entrypoint', opts['entrypoint'])
         if opts.get('entrypoint', False) else tuple()),
        chain.from_iterable(('-v', ':'.join(v)) for v in vmounts),
        chain.from_iterable(('-e', e) for e in opts['env']),
        (opts['image'],),
    ))

    return cmd, argv


# NOTE May break on big outputs, see
# https://stackoverflow.com/questions/31833897/python-read-from-subprocess-stdout-and-stderr-separately-while-preserving-order#31867499
def logs(container_name):
    return util.check_output(True, ('docker', 'logs', container_name))


def cleanup_procdir(container_names):
    '''Remove unsucessful tempdirs from worker exiting incorrectly'''
    for container_name in container_names:
        *_, job_id, node_hash = container_name.split('-')
        for proc_dir in DATADIR.glob(f'{node_hash}-*'):
            shutil.rmtree(proc_dir, ignore_errors=True)


def cleanup_running_containers():
    '''Cleanup exited docker containers

    :return: list of removed container identifiers
    '''
    container_names = running_processors()
    kill(container_names)
    cleanup_procdir(container_names)

    return container_names


def cleanup_stale_containers():
    '''Cleanup exited docker containers

    :return: list of removed container identifiers
    '''
    cmd_output = util.check_stdout(('docker', 'ps', '-a', '-f',
                                    'name=dagger-', '--format', '{{.Names}}'))
    containers = tuple(c for c in cmd_output.split('\n') if c)
    states = (inspect_state(c) for c in containers)
    stale_containers = [c for c, s in zip(containers, states)
                        if not s['Running']]
    wipe(stale_containers)
    cleanup_procdir(containers)

    return stale_containers
