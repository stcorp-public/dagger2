from daggerpy import state


class ProcessorError(Exception):
    def __init__(self, msg, state=state.FAILED):
        super().__init__(msg)
        self.state = state


class DockerRunError(ProcessorError):
    def __init__(self, stderr, returncode):
        '''Docker run system error

        :param stderr: stderr from docker run subprocess.
        :param returncode: returncode from docker run subprocess.
        '''

        super().__init__(f'Docker run exited ({returncode}): {stderr}')
        self.stderr = stderr


class InsufficientMemory(ProcessorError):
    def __init__(self, worker_mem_limit, processor_mem_limit):
        '''Insufficient worker memory

        :param worker_mem_limit: Dagger worker memory limit.
        :param processor_mem_limit: Memory limit set in processor definition.
        '''
        msg = ('Processor mem requirement exceeds available worker memory! '
               f'{worker_mem_limit:,} MB (worker) < {processor_mem_limit:,} MB '
               '(processor)')
        super().__init__(msg)


class DockerOOMKilled(ProcessorError):
    def __init__(self, container_name, container_mem):
        msg = (f'{container_name} Killed due to insufficient memory '
               f'({container_mem:,} MB) allocated to processor node '
               '(OOMKilled)! - Increase `mem_mb` in job graph specification!')
        super().__init__(msg, state=state.OOMKILLED)


class DockerFail(ProcessorError):
    def __init__(self, container_info, stderr):
        '''Docker container failure

        :param container_info: object from docker inspect.
        :param stderr: stderr from docker logs
        '''
        exitcode = container_info['State']['ExitCode']
        super().__init__(f'Container failed ({exitcode}):\n{stderr}')
        self.container_info = container_info
