#!/usr/bin/env python3
"""Node/function evaluation."""

import os
import subprocess
import datetime
import shutil
import traceback

from json.decoder import JSONDecodeError

from toolz import compose
from toolz.curried import map, filter, reduce

from daggerpy import graphql, docker, util, exceptions, state
from daggerpy.primitives import is_file, get_primitives
from daggerpy.settings import DATADIR
from daggerpy.types import Awaited, Aborted, Failed, Ordered


# List of fn's that does not maintain state (for optimization)
STATELESS = (
    'id',
    'set',
    'get',
    'order',
    'merge'
)


def node_evaluator(hostname, client, cache, logger, root=None):
    """Returns a function that evaluates a node."""

    # Node evaluations with hostname=None indicates that the evaluation
    # is not bound to disk and will provoke a dry run.
    dry_run = hostname is None

    # Since we are effectively doing a DFS and deep recursions, maintain a
    # local cache. This ensures that every node is evaluated at most
    # once per graph traversal. The function below decorates the
    # evaluating functions.
    def local_cache(f):
        def cached_eval(node_uid):
            if cache.get(node_uid) is None:
                output = f(node_uid)
                if not isinstance(output, Awaited):
                    cache[node_uid] = output
                return output
            return cache[node_uid]
        return cached_eval

    # The primitive function set does not maintain state.
    primitives = get_primitives(dry_run)

    def check_cache(finished_results, node, container_name, node_hash):
        ok_results = tuple(r for r in finished_results if r['exitcode'] == 0)

        if not ok_results:
            return

        result_uid = ok_results[0]['uid']
        invalid_files = tuple(invalid_result_files(client, result_uid))

        if not invalid_files:
            logger("[%s] <%s> cache hit on docker %s"
                   % (root, node['uid'], container_name))
            node['result'] = {'uid': result_uid, 'dgraph.type': 'Result'}
            node['started'] = datetime.datetime.utcnow().isoformat()
            graphql.set_node(client, node)
            return graphql.get_result(client, result_uid)

        for file_obj, (f_crc, f_bytes) in invalid_files:
            f_name = f'{file_obj["crc"]}-{file_obj["filename"]}'

            if isinstance(f_crc, FileNotFoundError):
                logger("[%s] <%s> cache (%s) file missing: %s"
                       % (root, node['uid'], node_hash, str(DATADIR / f_name)))
                continue

            logger("[%s] <%s> cache file %s invalid! CRC: %s, expected CRC: %s"
                   % (root, node['uid'], str(DATADIR / f_name), f_crc,
                      file_obj['crc']))

        graphql.del_result(client, result_uid, root)
        logger("[%s] <%s> re-running docker %s" % (root, node['uid'],
                                                   container_name))

    # Other functions (the 'docker' function and higher-order functions) can
    # mutate the node. These evaluations are transactional and maintain node
    # state.
    def mutating_functions(node_uid):

        # Docker node implementation (needs refactoring).
        # The docker function is the only function that can produce result
        # nodes.
        def _docker(kwargs):
            # Sanitize and perform checks on arguments
            opts = docker.sanitize_opts(**kwargs)
            if dry_run:
                return docker.result_sample
            docker.assert_image_opts(opts)

            # Fetch data from dgraph and check if already done.
            result = graphql.get_node_result(client, node_uid)
            if result is not None and result.get('exitcode') is not None:
                logger("[%s] <%s> docker is done" % (root, node_uid))
                return graphql.get_result(client, result['uid'])

            # Get state of containers from docker daemon and update worker state
            worker_state = graphql.get_worker_state(client, hostname)
            container_ids = docker.ps()
            docker_info = docker.inspect(container_ids)
            worker_state['running'] = compose(
                len,
                tuple,
                filter(lambda d: d['State']['Running'])
            )(docker_info)
            worker_state['mem_mb'] = compose(
                lambda mem_tot: int(mem_tot / 1e6),  # TODO MiB or MB?
                lambda mems: reduce(lambda x, y: x+y, mems, 0),
                map(lambda d: d['HostConfig']['Memory']),
                filter(lambda d: d['State']['Running'])
            )(docker_info)
            graphql.update_worker_state(client, worker_state)

            # Hash opts and generate name.
            # Environment variables are not included in hashing.
            node = graphql.get_node(client, node_uid)
            env = opts.pop('env')
            node_hash = util.hasher(opts)
            proc_dir = DATADIR / f'{node_hash}-{root}'

            # Get container info
            container_name = '-'.join(('dagger', node['name'],
                                       root or 'unknown', node_hash))

            matching_containers = tuple(sorted(
                (c for c in docker_info
                 if c['Name'].lstrip('/').startswith(container_name)),
                key=lambda c: c['Name']
            ))

            if len(matching_containers) == 0:
                container_info = None
            elif len(matching_containers) == 1:
                container_info = matching_containers[0]
            else:
                raise exceptions.ProcessorError(
                    f'Multiple containers with shared name: {container_name}!')

            container_mem_mb = int(opts['mem_mb'])

            if container_info is None:
                # Query for docker nodes that has the hash. Upon success, there
                # are three cases to handle:
                #  1. It is running.
                #  2. It finished successfully, for which it can be cached.
                #  3. It finished with a non-zero exit, for which it can be
                #     either cached or processed again.
                #
                # Here, when querying for dockers with the same hash, we may
                # restrict the search to the current worker or across all
                # worker graphs. Upon cache hit on different workers, files must
                # be resolved (e.g. over http). Upon cache hit on same worker,
                # just add a predicate to the result node.
                # TODO Caching:
                #   - search across all graphs and filter on total file size of
                #     docker file results.
                results = graphql.query_worker_results(
                    client,
                    hostname,
                    node_hash
                )
                finished_results = tuple(filter(
                    lambda res: res.get('exitcode') is not None,
                    results
                ))
                ongoing_results = tuple(filter(
                    lambda res: res.get('exitcode') is None,
                    results
                ))

                if finished_results and opts['cache'] in ('1', 'yes', 'true'):
                    cached = check_cache(finished_results, node, container_name,
                                         node_hash)

                    if cached is not None:
                        return cached

                elif ongoing_results:
                    return Awaited()

                # The docker can not be cached and should thus be started.
                # Check resources, wait if necessary.
                if worker_state['max_mem_mb'] < container_mem_mb:
                    raise exceptions.InsufficientMemory(
                        worker_state['max_mem_mb'],
                        container_mem_mb
                    )
                free_mem = worker_state['max_mem_mb'] - worker_state['mem_mb']
                if free_mem < container_mem_mb:
                    # # TODO: logging should be debug only, too spammy in prod
                    # logger("<%s> waiting for available memory" % node['uid'])
                    return Awaited()
                slots = worker_state['max_running'] - worker_state['running']
                if slots < 1:
                    # # TODO: logging should be debug only, too spammy in prod
                    # logger("<%s> waiting for open slot" % node['uid'])
                    return Awaited()

                # Start the container.

                logger("[%s] <%s> create tempdir %s"
                       % (root, node['uid'], proc_dir))

                proc_dir.mkdir()

                # TODO Can't get stdin to work with detached mode (which is
                # not really strange). When stdin is given, run synchronously.
                cmd, stdin = docker.docker_run_subprocess_args(
                    container_name, opts, env, proc_dir)
                logger("[%s] <%s> docker run: %s"
                       % (root, node['uid'], repr(cmd)))
                node['state'] = state.RUNNING
                node['started'] = datetime.datetime.utcnow().isoformat()
                graphql.set_node(client, node)
                proc = subprocess.run(
                    cmd,
                    input=stdin,
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.PIPE,
                    close_fds=True
                )

                if proc.returncode != 0:
                    shutil.rmtree(proc_dir)

                    try:
                        container_state = docker.inspect_state(container_name)
                    except JSONDecodeError:
                        container_state = {'OOMKilled': False}

                    if container_state['OOMKilled']:
                        raise exceptions.DockerOOMKilled(container_name,
                                                         container_mem_mb)

                    stderr = proc.stderr.decode().rstrip()
                    logger("[%s] <%s> docker run ERROR! - %s" %
                           (root, node['uid'], proc.stderr.decode().rstrip()))
                    raise exceptions.DockerRunError(stderr, proc.returncode)

                # Bind result node
                node['result'] = {
                    'hash': node_hash,
                    'dgraph.type': 'Result'
                }
                graphql.set_node(client, node)

                # Update worker state immediately
                worker_state['mem_mb'] += container_mem_mb
                worker_state['running'] += 1
                graphql.update_worker_state(client, worker_state)
                return Awaited()

            if container_info['State']['Running']:
                # # TODO: logging should be debug only, too spammy in prod
                # logger("<%s> docker still running" % node['uid'])
                return Awaited()

            # Check results.
            exitcode = container_info['State']['ExitCode']
            logger("[%s] <%s> docker %s exited (%d) (%s)" % (
                root,
                node['uid'],
                container_name,
                exitcode,
                container_info['Id']
            ))

            # Resolve files and/or directories with docker cp.
            # Only resolve file output on exitcode 0.
            file_nodes = []
            if exitcode == 0:
                input_files = tuple(f['filename'] for f in opts['argv']
                                    if is_file(f))

                for fp in util.proc_files(proc_dir):
                    # ensure input files are not treated as output files
                    # TODO: Verify if still needed w/o docker cp
                    if fp.name in input_files:
                        continue

                    crc, nbytes = util.cksum(str(fp))
                    filename = fp.name

                    target_path = DATADIR / f'{crc}-{filename}'
                    # If the file already exists, bind existing file obj
                    file_obj = graphql.query_worker_file(
                        client, hostname, filename, crc
                    )

                    if file_obj:
                        util.informed_assert(
                            target_path.is_file(),
                            "File indexes in dgraph is not on disk"
                        )
                        logger("[%s] <%s> deduplicate file %s" %
                               (root, node['uid'], filename))
                        file_nodes.append(file_obj)
                    # Otherwise move it to flat storage
                    else:
                        logger("[%s] <%s> add %s to file store" %
                               (root, node['uid'], target_path))

                        try:
                            # NOTE: Hardlink ensures fast move!
                            # XXX: Assumes same partition!
                            os.link(fp, target_path)
                        except FileExistsError:
                            pass

                        worker_state['disk_usage'] += nbytes
                        file_nodes.append({
                            'filename': filename,
                            'bytes': nbytes,
                            'crc': crc,
                            'dgraph.type': 'File'
                        })

            if exitcode == 0 and len(file_nodes) == 0:
                logger('[%s] <%s> (%s) WARNING: NO OUTPUT FILES! Incorrect '
                       'output location in container if it produces files!'
                       % (root, node['uid'], node['name']))

            # Update result.
            # Make sure that the result node is there (in case dagger goes
            # down and containers continue).
            if result is None:
                node['result'] = {
                    'hash': node_hash,
                    'dgraph.type': 'Result'
                }
                # Early commit decreases chances of inconsistencies
                graphql.set_node(client, node)
                result = graphql.get_node_result(client, node_uid)

            stdout, stderr = docker.logs(container_name)
            result = {
                'uid': result['uid'],
                'hash': node_hash,
                'exitcode': exitcode,
                'log': "",  # TODO
                'stdout': stdout,
                'stderr': stderr,
                'files': file_nodes,
                'job': {'uid': root}
            }
            node['result'] = result
            graphql.set_node(client, node)

            # Bind result to worker
            result_node = graphql.get_node_result(client, node_uid)

            if not worker_state.get('results'):
                worker_state['results'] = []

            worker_state['results'].append(result_node)
            graphql.update_worker_state(client, worker_state)

            # Remove container
            subprocess.check_call(('docker', 'rm', '-v', container_name))
            shutil.rmtree(proc_dir)

            logger("[%s] <%s> docker finished (%d) (%s)"
                   % (root, node['uid'], exitcode, container_info['Id']))

            if container_info['State']['OOMKilled']:
                raise exceptions.DockerOOMKilled(container_name,
                                                 container_mem_mb)

            # Just fail the whole job upon non-zero exit for now.
            if exitcode != 0:
                raise exceptions.DockerFail(container_info, stderr)

            # We can either just return the output directly or re-fetch node.
            return graphql.get_result(client, result['uid'])

        # Map takes two nodes as inputs:
        #  1. A node/subgraph that has exactly one input.
        #  2. A list.
        def _map(f_uid, a):
            node = graphql.get_node(client, node_uid)
            # Check that a is not empty
            util.informed_assert(
                len(a) > 0,
                "List can not be empty"
            )

            # Find the input node of the subgraph f_uid. There should be
            # exactly one.
            # Note: There are several ways of matching the target input node
            # to each list element. The current way is the most "automatic"
            # but restricts the subgraph to having exactly one input. Another
            # way is to match by name, i.e. that each element of the list has
            # the same name and is matched to a node with the same name in f.
            # Note 2: To deal with scoping, that is to allow the subgraph f
            # to see nodes in the higher scope (that is already eval'ed), filter
            # input nodes on uid's that are already in the cache.
            input_nodes = tuple(filter(
                lambda i: i['uid'] not in cache.keys(),
                graphql.query_input_nodes(client, f_uid)
            ))
            logger("[%s] <%s> inputs for subgraph <%s>: %s" % (
                root,
                node['uid'],
                f_uid,
                repr(tuple('<%s>' % i['uid'] for i in input_nodes))
            ))
            util.informed_assert(
                len(input_nodes) == 1,
                "Found %d inputs to map, expected 1" % len(input_nodes)
            )
            in_uid = input_nodes[0]['uid']
            in_node = graphql.get_node(client, in_uid)

            # Inspect input nodes (f and a)
            node = graphql.get_node(client, node['uid'])
            order_nodes = map(
                graphql.get_node(client),
                (i['uid'] for i in node['input'])
            )
            f_order, a_order = sorted(order_nodes, key=lambda n: n['name'])

            # Get facets bound to f
            f_facets = dict(filter(
                lambda item: item[0].startswith('input|'),
                f_order['input'][0].items()
            ))
            # XXX: Simplify
            # f_facets = {k: v for k, v in f_order['input'][0].items()
            #             if k.startswith('input|')}
            logger("[%s] <%s> output of <%s> has facets %s" % (
                root,
                node['uid'],
                f_uid,
                str(f_facets)
            ))

            # Get the uid of a and assert that a is list type
            a_uid = a_order['input'][0]['uid']
            logger("[%s] <%s> a has uid <%s>" % (root, node['uid'], a_uid))
            util.informed_assert(
                isinstance(a, list),
                "Second argument to map is not list, but '%s'" % str(type(a))
            )

            # Type evaluate f(a[0]).
            # Apply a[0] and add the result to the local cache, then evaluate
            # f normally.
            if dry_run:
                in_node['input'] = [{
                    'uid': '_',
                }]
                cache['_'] = a[0]
                cache[in_uid] = call_node(in_node)
                del cache['_']
                return [eval_node(f_uid)]

            # Mutate the subgraph defined by the map node.
            # For each a[i], clone f and bind input node, and list all
            # f[i] outputs.
            def clone_and_bind_ai(i):
                f_clone_uid = graphql.clone_graph(client, f_uid)
                in_node = graphql.get_node(
                    client,
                    graphql.query_input_nodes(client, f_clone_uid)[0]['uid']
                )
                in_node['input'] = [{
                    'fn': 'get',
                    'name': str(i),
                    'dgraph.type': 'Node',
                    'input': [{
                        'uid': a_uid
                    }]
                }]
                logger("[%s] <%s> clone f (<%s> -> <%s>) and bind input %d" % (
                    root, node['uid'], f_uid, f_clone_uid, i
                ))
                graphql.set_node(client, in_node)
                # Make sure to set original facets on f_clone
                f_clone = {**f_facets,
                           **{'uid': f_clone_uid, 'input|eval': True}}
                return {
                    'fn': 'order',
                    'name': str(i),
                    'dgraph.type': 'Node',
                    'input': [f_clone]
                }

            node['fn'] = 'list'

            # The map node is now mutating into an evaluated list node. Hence
            # the input edge to the old subgraph of the previous node state is
            # removed. However, it is currently unsafe to remove the old
            # subgraph, as this can lead to a bug where where correctly
            # evaluated graph state gets deleted. This bug is demonstrated in
            # the test graph:
            # `tests/integration/failing/test_map_clone_input_corruption.py`.
            graphql.del_input_edges(client, node['uid'])

            node['input'] = list(map(clone_and_bind_ai, range(len(a))))
            graphql.set_node(client, node)

            # TODO: cleanly remove old subgraph, without corrupting valid state
            # of previously correct graph evaluation. See details in previous
            # comment.

            # On the next pass, the newly mutated list node will evaluate all
            # subgraphs. Finally, the map node evaluates itself
            return eval_node(node['uid'])

        return {
            'docker': _docker,
            'map': _map
        }

    def eval_edge(i):
        if not i.get('input|eval', True):
            return i['uid']

        # Evaluate and check for Failed or Aborted. If the maybe monad is set,
        # a value of None is returned (which is filtered from inputs).
        output = eval_node(i['uid'])
        failed = isinstance(output, Failed) or isinstance(output, Aborted)

        if root is not None and graphql.killing_job(client, root):
            return output

        if failed and hostname and i.get('input|monad') == 'maybe':
            return None

        return output

    def call_node(node):
        # Recurse on input edges sequentially in uid order. Ignore all
        # None-valued inputs.
        inputs = tuple(eval_edge(i) for i in node.get('input', []))

        # Inputs may be unordered (as edges appear in dgraph -- a set) or
        # ordered.
        # TODO Either all inputs should be ordered or none -- check
        def arg_sort_key(x):
            if isinstance(x, Ordered):
                return x.index
            return 9999

        def maybe_unpack(x):
            if isinstance(x, Ordered):
                return x.value
            return x

        args = tuple(e for e in map(maybe_unpack,
                                    sorted(inputs, key=arg_sort_key))
                     if e is not None)

        # Any inputs of type Awaited indicates that the subgraph is processing.
        if tuple(filter(lambda x: isinstance(x, Awaited), inputs)):
            return Awaited()

        # Any inputs of type Failed or Aborted provokes an Aborted exception.
        if tuple(filter(lambda x: isinstance(x, Failed), inputs)):
            return Aborted()
        if tuple(filter(lambda x: isinstance(x, Aborted), inputs)):
            return Aborted()

        # The get, set and index functions are special -- we (ab)use the "name"
        # field for the dpath/index. Not sure whether this is a good idea.
        # These functions does not fail on empty args in order to propagate
        # maybe's.
        fn = node.get('fn', 'id')
        if fn in ('set', 'get', 'order'):
            if not args:
                return None
            return primitives[fn](node.get('name', ''), *args)

        # If a leaf, the node must be fn=id with a literal predicate.
        if not args:
            # XXX: Use dgraph.type != 'Leaf' instead of fn != 'id'?
            if fn != 'id':
                raise Exception("Missing input(s)")
            if node.get('literal') is not None:
                return node['literal'] if hostname else ""
            else:
                raise Exception(f'Leaf node ({node}) has no literal!')

        # Eval other primitives.
        if fn in primitives:
            return primitives[fn](*args)

        # Get set of mutating functions and eval.
        if not node.get('name'):
            raise Exception("Mutating functions must be named")
        dirty_fn_set = mutating_functions(node['uid'])
        if fn not in dirty_fn_set:
            raise Exception("No function %s" % fn)
        return dirty_fn_set[fn](*args)

    def maybe_init_node(node):
        """Set the initial node state if the node is eligble to maintain state.
        Returns the updated node.
        """
        needs_state = (hostname
                       and node.get('fn', 'id') not in STATELESS
                       and node.get('state') is None)

        if needs_state:
            node['dgraph.type'] = 'State'
            node['state'] = state.EVALUATING
            graphql.set_node(client, node)

        return node

    def maybe_set_final_state(node_state, node):
        """Set final node state if node is stateful."""
        needs_final_state = (hostname
                             and 'State' in node.get('dgraph.type', set()))

        if needs_final_state:
            killing_job = root is not None and graphql.killing_job(client, root)
            node['state'] = state.KILLED if killing_job else node_state

            node['finished'] = datetime.datetime.utcnow().isoformat()
            graphql.set_node(client, node)

    @local_cache
    def eval_node(node_uid):
        # Fetch node and format logging prefix
        n = graphql.get_node(client, node_uid)
        node = maybe_init_node(n)
        fn = node.get('fn', 'id')
        prelog = "<%s> %s, fn='%s', input=%s" % (
            node_uid,
            node.get('name', '<unnamed>'),
            fn,
            repr(tuple('<%s>' % i['uid'] for i in node.get('input', [])))
        )

        # TODO: cleanup proc_dirs in exception handling

        # Evaluate node object. Capture any exceptions and propagate dedicated
        # failure types instead of re-raising up the stack.
        try:
            output = call_node(node)
        except exceptions.DockerRunError as e:
            node = graphql.get_node(client, node_uid)
            node['error'] = e.stderr
            maybe_set_final_state(e.state, node)
            logger(f'[{root}] {prelog} -> {state.FAILED}: {e}')
            logger(traceback.format_exc())
            return Failed()
        except (exceptions.DockerOOMKilled, exceptions.InsufficientMemory) as e:
            node = graphql.get_node(client, node_uid)
            node['error'] = str(e)
            maybe_set_final_state(e.state, node)
            logger(f'[{root}] {prelog} -> {state.FAILED}: {e}')
            logger(traceback.format_exc())
            return Failed()
        except exceptions.ProcessorError as e:
            maybe_set_final_state(e.state, graphql.get_node(client, node_uid))
            logger(f'[{root}] {prelog} -> {state.FAILED}: {e}')
            logger(traceback.format_exc())
            return Failed()
        except Exception as e:
            node = graphql.get_node(client, node_uid)

            if 'State' in node['dgraph.type']:
                node['error'] = traceback.format_exc()

            maybe_set_final_state(state.FAILED, node)
            logger(f'[{root}] {prelog} -> {state.FAILED}: {e}')
            logger(traceback.format_exc())
            return Failed()

        if not isinstance(output, Awaited) and 'State' in node['dgraph.type']:
            node_failed = (any(isinstance(output, f) for f in (Aborted, Failed))
                           and 'State' in node['dgraph.type'])
            node_state = state.ABORTED if node_failed else state.DONE
            out_preview = str(output)

            if len(out_preview) > 100:
                out_preview = f'{out_preview[:100]} ...'

            maybe_set_final_state(node_state,
                                  graphql.get_node(client, node_uid))
            logger("[%s] %s -> %s" % (root, prelog, out_preview))

        return output

    return eval_node


def validate_file(f):
    '''Validates dgraph file object'''
    util.informed_assert(is_file(f), "Not a file object")
    return f


def invalid_result_files(client, result_uid):
    '''Return invalid cached files on disk'''

    for file_obj in graphql.get_result_files(client, result_uid):
        file_on_disk = DATADIR / f'{file_obj["crc"]}-{file_obj["filename"]}'

        if not file_on_disk.exists() or not file_on_disk.is_file():
            yield (file_obj, (FileNotFoundError(), None))
            continue

        f_crc_bytes = util.cksum(str(file_on_disk))

        if f_crc_bytes != (file_obj['crc'], file_obj['bytes']):
            yield (file_obj, f_crc_bytes)


def docker_result_resolver(client, root_node_uid):
    '''Resolves prior execution of completed job graphs'''

    if graphql.get_node(client, root_node_uid).get('fn') != 'docker':
        raise ValueError('Root node must be a docker function!')

    return result_resolver(client, root_node_uid)


def result_resolver(client, root_node_uid):
    '''Resolves prior execution of arbitrary root node'''

    primitives = get_primitives(False)
    # We allow file paths to not exist on the file system
    primitives['file'] = validate_file
    primitives['files'] = lambda l: list(map(validate_file, l))

    def resolve(node_uid, monad=''):
        node = graphql.get_node(client, node_uid)
        result = node.get('result')

        # root node results lacks data required to resolve the node execution
        if node_uid != root_node_uid and result:
            if monad == 'maybe' and node['state'] != state.DONE:
                return None

            return graphql.get_result(client, result['uid'])

        inputs = tuple(resolve(i['uid'], i.get('input|monad', ''))
                       for i in node.get('input', []))

        # Inputs may be unordered when fetched from dgraph, otherwise ordered.
        # TODO Either all inputs should be ordered or none -- check
        def arg_sort_key(x):
            if isinstance(x, Ordered):
                return x.index
            return 9999

        def maybe_unpack(x):
            if isinstance(x, Ordered):
                return x.value
            return x

        args = tuple(a for a in map(maybe_unpack,
                                    sorted(inputs, key=arg_sort_key))
                     if a is not None)
        fn = node.get('fn', 'id')

        if fn == 'docker' and node['uid'] == root_node_uid:
            return args[0]  # evaluation completed

        # these nodes represent edge data, node name has special meaning
        if fn in ('set', 'get', 'order'):
            if not args:
                return None

            try:
                r = primitives[fn](node.get('name', ''), *args)
            except Exception as e:
                msg = f'Primitive `{fn}` FAILED in {node["uid"]}: {e}'
                raise RuntimeError(msg)

            return r

        # If leaf, node must be fn=id with a literal predicate.
        if not args:
            if fn != 'id':
                raise Exception(f'Missing input(s) in node: {node["name"]}')

            if node.get('literal') is not None:
                return node['literal']
            else:
                raise Exception("Node is leaf but has no literal")

        if fn in primitives:
            return primitives[fn](*args)

    return resolve
