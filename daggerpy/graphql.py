import json
import pydgraph

from toolz import curry, compose
from daggerpy import util
from daggerpy import state


def create_client(graphql_server):
    '''Create a dgraph client object

    :param graphql_server: <host>:<port> to Dgraph Alpha server.
    :type graphql_server: str
    '''
    client_stub = pydgraph.DgraphClientStub(graphql_server)
    # graphql_server, None, {'grpc.max_receive_message_length': 1073741824})
    return pydgraph.DgraphClient(client_stub)


def query_nodes_by_name(client, name):
    query = """query nodes($name: string)
    {
        nodes(func: eq(name, $name)) {
            uid
            expand(_all_)
        }
    }"""
    res = client.txn(read_only=True).query(query, variables={'$name': name})
    return json.loads(res.json)


def query_job_files(client, hostname, job_uid):
    query = '''query var($job_uid: string, $hostname: string)
    {
        var(func: eq(hostname, $hostname)) @cascade {
            R as results {
                job @filter(uid($job_uid)) {}
            }
        }
        var(func: uid(R)) {
            F as files {}
        }
        jobfiles(func: uid(F)) {
            filename
            crc
            bytes
        }
    }'''
    variables = {'$hostname': hostname, '$job_uid': job_uid}
    res = client.txn(read_only=True).query(query, variables=variables)

    try:
        files = json.loads(res.json)['jobfiles']
    except Exception:
        return []

    return files


def query_job_dockers(client, job_uid):
    query = """query nodes($job_uid: string)
    {
        var(func: uid($job_uid)) @recurse {
            D as uid
            fn
            input
            literal
            files
            result
            hash
            exitcode
            started
            finished
            state
            filename
        }
        dockernodes(func: uid(D)) @filter(eq(fn, "docker")) @recurse {
            uid
            name
            hash
            literal
            result
            exitcode
            started
            finished
            state
            filename
            fn
        }
    }"""
    variables = {'$job_uid': job_uid}
    res = client.txn(read_only=True).query(query, variables=variables)
    return json.loads(res.json)['dockernodes']


@curry
def get_node(client, uid):
    # TODO will return a uid result regardless of whether it exists or not!
    # Is this the pydgraph client acting up or dgraph standard behavior?
    query = """{
        nodes(func: uid(%s)) {
            uid
            dgraph.type
            expand(_all_) {
                uid
            }
        }
    }""" % uid
    res = client.txn(read_only=True).query(query, variables=None)
    nodes = json.loads(res.json)['nodes']
    if not nodes:
        raise Exception("<%s> node not found" % uid)
    return nodes[0]


def get_full_node(client, uid):
    query = """{
        nodes(func: uid(%s)) {
            uid
            fn
            name
            doc
            literal
            dgraph.type
            input @facets {
                uid
                fn
                name
                literal
                dgraph.type
            }
            state
            error
            virt_mem_peak_kb
            res_mem_peak_kb
            started
            finished
            result {
                hash
                exitcode
                stdout
                stderr
                log
                dgraph.type
                files {
                    filename
                    crc
                    bytes
                    dgraph.type
                }
            }
        }
    }""" % uid
    res = client.txn(read_only=True).query(query, variables=None)
    nodes = json.loads(res.json)['nodes']
    if not nodes:
        raise Exception("<%s> node not found" % uid)
    return nodes[0]


def get_stripped_node(client, uid):
    query = """{
        nodes(func: uid(%s)) {
            uid
            input {
                uid
            }
        }
    }""" % uid
    res = client.txn(read_only=True).query(query, variables=None)
    nodes = json.loads(res.json)['nodes']
    if not nodes:
        raise Exception("<%s> node not found" % uid)
    return nodes[0]


def set_node(client, node):
    txn = client.txn()

    try:
        result = txn.mutate(set_obj=node, commit_now=True)
    finally:
        txn.discard()

    return result


def killing_job(client, job_uid):
    q = '''query job($uid: string)
    {
        job(func: uid($uid)) {
            state
        }
    }'''
    result = client.txn(read_only=True).query(q, variables={'$uid': job_uid})

    try:
        job, = json.loads(result.json)['job']
    except (ValueError, KeyError):
        return False

    return job['state'] == state.KILLING


def kill_job(client, uid):
    txn = client.txn()
    cond = '@if(eq(len(J), 1))'
    nquad = f'uid(J) <state> "{state.KILLING}" .'
    mutation = txn.create_mutation(set_nquads=nquad, cond=cond)
    q = ('{ J as var(func: uid(%s)) '
         '@filter(anyofterms(state, "%s")) }') % (uid, ' '.join(state.ONGOING))
    request = txn.create_request(mutations=[mutation], query=q, commit_now=True)

    try:
        txn.do_request(request)
    finally:
        txn.discard()


def del_node(client, uid, recurse=False):
    seen = set()

    def traverse(node_uid):
        if node_uid in seen:
            return

        node = get_stripped_node(client, node_uid)

        if recurse:
            for child in node.get('input', []):
                traverse(child['uid'])

            del_input_edges(client, node_uid)

        seen.add(node_uid)
        txn = client.txn()

        try:
            txn.mutate(del_obj={'uid': node_uid}, commit_now=True)
        finally:
            txn.discard()

    traverse(uid)


def del_input_edges(client, uid):
    txn = client.txn()

    try:
        txn.mutate(del_nquads='<%s> <input> * .' % uid, commit_now=True)
    finally:
        txn.discard()


def del_job_edge(client, worker_uid, node_uid):
    txn = client.txn()
    nquad = f'<{worker_uid}> <jobs> <{node_uid}> .'

    try:
        result = txn.mutate(del_nquads=nquad, commit_now=True)
    finally:
        txn.discard()

    return result


def jobspec(client, name):
    query = """query g($name: string)
    {
        g(func: type(JobSpec)) @filter(eq(name, $name)) {
            uid
            fn
            name
            input {
                uid
            }
        }
    }"""
    res = client.txn(read_only=True).query(query, variables={'$name': name})

    try:
        jobspec, = json.loads(res.json)['g']
    except (ValueError, KeyError):
        return None

    return jobspec


def clone_graph(client, uid):
    # TODO it should be safer to manually traverse the graph by fetching each
    # node upon visit instead of doing a recurse query since the returned
    # dict can become very deep.
    query = """{
        graph(func: uid(%s)) @recurse(depth: 1000, loop: true) {
            uid
            dgraph.type
            fn
            name
            literal
            input @facets
        }
    }""" % uid
    res = client.txn(read_only=True).query(query)
    graph = json.loads(res.json)['graph'][0]

    # Traverse the graph and de-duplicate common source nodes. Accumulate
    # nquads along the way.
    uidmap = {}
    nquads = set()
    add_nquad = compose(nquads.add, util.fmt_nquad)

    # TODO we can effectively check for DAG criteria here -- just maintain
    # a set of visited nodes with respect to the current node in the DFS
    def traverse_node(node):
        if node['uid'] in uidmap.keys():
            return uidmap[node['uid']]

        # Set unique rdfid and add to uidmap
        rdfid = 'clone%d' % len(uidmap.keys())
        uidmap[node['uid']] = rdfid
        # Add common predicates
        for k in ('fn', 'name', 'literal'):
            if node.get(k) is not None:
                add_nquad(rdfid, k, node[k])

        for t in node['dgraph.type']:
            add_nquad(rdfid, 'dgraph.type', t)

        # Recurse and add input predicates
        for child in node.get('input', []):
            child_id = traverse_node(child)
            facets = dict(map(
                lambda k: (k.split('|')[-1], child[k]),
                filter(lambda s: s.startswith('input|'), child.keys())
            ))
            add_nquad(rdfid, 'input', child_id, facets)
        return rdfid

    # Return the assigned uid of the top node
    top_rdfid = traverse_node(graph)
    txn = client.txn()
    assigned = txn.mutate(set_nquads='\n'.join(nquads), commit_now=True)
    return assigned.uids[top_rdfid]


def del_type(client, uid, t):
    query = """{
        n as var(func: uid(%s)) {
            uid
        }
    }""" % uid
    txn = client.txn()
    del_nquad = f'uid(n) <dgraph.type> "{t}" .'
    del_m = txn.create_mutation(del_nquads=del_nquad, cond='@if(eq(len(n), 1))')
    request = txn.create_request(query=query, mutations=[del_m],
                                 commit_now=True)

    try:
        txn.do_request(request)
    finally:
        txn.discard()


def set_type(client, uid, t):
    query = """{
        n as var(func: uid(%s)) {
            uid
        }
    }""" % uid
    txn = client.txn()
    o = {
        'uid': uid,
        'dgraph.type': t
    }
    set_m = txn.create_mutation(set_obj=o, cond='@if(eq(len(n), 1))')
    request = txn.create_request(query=query, mutations=[set_m],
                                 commit_now=True)

    try:
        txn.do_request(request)
    finally:
        txn.discard()


def del_graph(client, root_uid):
    query = '''
    {
        var(func: uid(%s)) @recurse(loop: true, depth: 10000) {
            U as uid
            input
        }
    }''' % root_uid

    txn = client.txn()
    mutation = txn.create_mutation(del_nquads='uid(U) * * .')
    request = txn.create_request(mutations=[mutation], query=query,
                                 commit_now=True)
    txn.do_request(request)


def get_worker_state(client, hostname):
    query = """query worker($hostname: string)
    {
        worker(func: eq(hostname, $hostname)) {
            uid
            expand(_all_)
        }
    }"""
    variables = {'$hostname': hostname}
    res = client.txn(read_only=True).query(query, variables=variables)
    workers = json.loads(res.json)['worker']

    if workers:
        return workers[0]

    return None


def get_worker_jobs(client, hostname):
    query = """query worker($hostname: string)
    {
        worker(func: eq(hostname, $hostname)) {
            uid
            jobs {
                uid
            }
        }
    }"""
    variables = {'$hostname': hostname}
    res = client.txn(read_only=True).query(query, variables=variables)
    workers = json.loads(res.json)['worker']

    if workers:
        return workers[0]
    return None


def query_active_jobs(client, hostname):
    # Filter with negation query, node might not have state attribute
    query = """query worker($hostname: string)
    {
        worker(func: eq(hostname, $hostname)) {
            jobs @filter(not anyofterms(state, "%s")) {
                uid
                name
                state
                started
            }
        }
    }""" % " ".join(state.TERMINATED)
    variables = {'$hostname': hostname}
    res = client.txn(read_only=True).query(query, variables=variables)

    try:
        worker, = json.loads(res.json)['worker']
    except (KeyError, ValueError):
        return []

    return worker['jobs']


def query_all_jobs(client, hostname):
    query = """query worker($hostname: string)
    {
        worker(func: eq(hostname, $hostname)) {
            jobs {
                uid
                name
                state
                started
                finished
            }
        }
    }"""
    variables = {'$hostname': hostname}
    res = client.txn(read_only=True).query(query, variables=variables)
    workers = json.loads(res.json)['worker']

    return workers[0].get('jobs', []) if workers else []


def register_worker(client, hostname):
    q = """query worker($hostname: string)
    {
        var(func: eq(hostname, $hostname)) {
            NODE as uid
        }
    }"""
    txn = client.txn()
    state = {'hostname': hostname, 'dgraph.type': 'Worker'}
    m = txn.create_mutation(set_obj=state, cond='@if(eq(len(NODE), 0))')
    request = txn.create_request(query=q,
                                 mutations=(m,),
                                 variables={'$hostname': hostname},
                                 commit_now=True)

    try:
        txn.do_request(request)
    finally:
        txn.discard()


def update_worker_state(client, state):
    '''Updates state of existing worker

    Add worker from CLI: `dagger worker register <hostname>`
    '''

    # TODO: Avoid string interpolation using query variables.
    #       Cannot get this working with pydgraph 2.0.2
    q = '{ w as var(func: eq(hostname, "%s")) }' % state['hostname']

    # Upsert state
    state['uid'] = 'uid(w)'
    txn = client.txn()
    m = txn.create_mutation(set_obj=state, cond='@if(eq(len(w), 1))')
    request = txn.create_request(query=q, mutations=(m,), commit_now=True)

    try:
        txn.do_request(request)
    finally:
        txn.discard()


def get_node_result(client, uid):
    query = """{
        nodes(func: uid(%s)) {
            uid
            result {
                uid
                expand(_all_)
            }
        }
    }""" % uid
    res = client.txn(read_only=True).query(query, variables=None)

    try:
        node, = json.loads(res.json)['nodes']
    except (ValueError, KeyError):
        raise Exception("<%s> node not found" % uid)

    try:
        result = node['result']
    except KeyError:
        result = None

    return result


def get_result(client, uid):
    query = """{
        result(func: uid(%s)) {
            stdout
            stderr
            files {
                filename
                crc
                bytes
            }
        }
    }""" % uid
    res = client.txn(read_only=True).query(query, variables=None)

    try:
        results, = json.loads(res.json)['result']
    except Exception:
        results = None

    return results


def del_result(client, result_uid, job_uid):
    nquads = '\n'.join((f'<{result_uid}> <files> * .',
                        f'<{result_uid}> <hash> * .'))
    txn = client.txn()

    try:
        txn.mutate(del_nquads=nquads, commit_now=True)
    finally:
        txn.discard()


def get_result_files(client, uid):
    query = """{
        result(func: uid(%s)) {
            files {
                filename
                crc
                bytes
            }
        }
    }""" % uid
    res = client.txn(read_only=True).query(query, variables=None)

    try:
        result = json.loads(res.json)['result'][0]['files']
    except Exception:
        result = []

    return result


def query_worker_results(client, hostname, node_hash):
    query = """query worker($hostname: string, $hash: string)
    {
        worker(func: eq(hostname, $hostname)) {
            results @filter(eq(hash, $hash)) {
                uid
                exitcode
            }
        }
    }"""
    variables = {'$hostname': hostname, '$hash': node_hash}
    res = client.txn(read_only=True).query(query, variables=variables)
    workers = json.loads(res.json)['worker']

    if workers:
        return workers[0]['results']
    return []


def query_worker_file(client, hostname, filename, crc):
    query = """query worker($hostname: string, $filename: string, $crc: int)
    {
        worker(func: eq(hostname, $hostname)) {
            results {
                files @filter(eq(filename, $filename) AND eq(crc, $crc)) {
                    uid
                }
            }
        }
    }"""
    variables = {'$hostname': hostname, '$filename': filename, '$crc': str(crc)}
    res = client.txn(read_only=True).query(query, variables=variables)
    workers = json.loads(res.json)['worker']
    if workers:
        return workers[0]['results'][0]['files'][0]
    return None


def query_input_nodes(client, uid):
    """Traverse the subgraph defined by uid along input and filter
    all nodes that are not a literal data point, that can be evaluated, and does
    not have an input.
    """
    query = """{
        var(func: uid(%s))
          @recurse(depth: 1000, loop: true)
          @normalize
        {
            NODE AS uid
            input @facets(not eq(eval, false))
        }

        input_nodes(func: uid(NODE))
            @filter(not has(literal) and not has(input))
        {
            uid
        }
    }""" % uid
    res = client.txn(read_only=True).query(query, variables=None)
    return json.loads(res.json)['input_nodes']


def query_job_failure(client, job_uid):
    query = '''query var($job_uid: string)
    {
        var(func: uid($job_uid)) @recurse {
            D as uid
            fn
            input
            started
            finished
            state
        }
        dockerfailure(func: uid(D), first: 1)
            @filter(eq(fn, "docker") and anyofterms(state, "%s"))
        {
            uid
            name
            fn
            started
            finished
            error
            state
            result {
                stderr
                stdout
            }
        }
    }''' % ' '.join((state.FAILED, state.OOMKILLED))

    variables = {'$job_uid': job_uid}
    res = client.txn(read_only=True).query(query, variables=variables)

    try:
        failure = json.loads(res.json)['dockerfailure'][0]
    except Exception:
        return []

    return failure
