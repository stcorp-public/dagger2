import multiprocessing
import os
import sys

from dotenv import load_dotenv
from pathlib import Path
from socket import getfqdn
from warnings import warn


CONF_DIRS = (
    os.getenv('DAGGER_CONFIG_DIR'),
    '.',
    f'{os.getenv("XDG_CONFIG_HOME", "~/.config")}/dagger',
)

TRUTH = ('1', 'yes', 'on', 'true')


def config_file_path():
    '''Find path to potential .env'''
    paths = (Path(p).expanduser() / '.env' for p in CONF_DIRS if p)

    for p in paths:
        if p.is_file():
            return p


load_dotenv(config_file_path())

try:
    GRAPHQL_SERVER = os.environ['GRAPHQL_SERVER']
    GRAPHQL_HTTP_SERVER = os.environ['GRAPHQL_HTTP_SERVER']
except KeyError as missing_key:
    sys.exit(f'Configuration ERROR!\nMissing config: {missing_key}'
             ' Ensure correct .env file or set your envvars!')

DAGGER_WORKER = os.getenv('DAGGER_WORKER', getfqdn())

if 'localhost' in DAGGER_WORKER:
    warn('DAGGER_WORKER set to `localhost`. DO NOT USE IN PRODUCTION!')

N_CORES = multiprocessing.cpu_count()

try:
    DATADIR = Path(os.environ['DAGGER_DATADIR']).expanduser().absolute()
except KeyError:
    DATADIR = (Path('.') / 'data').absolute()

TEST_MODE = os.getenv('DAGGER_TEST_MODE', '').lower() in TRUTH
