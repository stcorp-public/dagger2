import uuid

from toolz import compose
from daggerpy.web import graph as web_graph
from daggerpy import graphql, util
from daggerpy.evaluate import node_evaluator
from daggerpy.types import Failed, Aborted
from daggerpy.settings import GRAPHQL_SERVER, DAGGER_WORKER


def get_rdfid(used_ids, base):
    """Get the next available ID for the given base string."""
    i = 0
    while base + str(i) in used_ids:
        i += 1
    return base + str(i)


class Input(object):
    """The Input class models directed links/edges between nodes and consists
    of a value and a set of facets. The value can be str, Node or another Input,
    allowing for edge composition in a monadic fashion, such that
    Input . Input (v) = Input (v)
    """
    def __init__(self, value, **facets):
        if isinstance(value, Input):
            self.value = value.value
            self.facets = {**value.facets, **facets}
        elif isinstance(value, Node) or isinstance(value, str):
            self.value = value
            self.facets = facets
        else:
            raise Exception("Invalid input value (type %s)" % str(type(value)))


class Node(object):
    """The Node class models nodes/vertices in the graph."""
    def __init__(self, name, fn='id', doc=None):
        self.uid = None
        self.name = name
        self.fn = fn
        self.doc = doc
        self.inputs = []
        self.rdfid = None
        self.nquads = set()

    def __call__(self, *inputs):
        """Note that the Node base class does not perform anything additional,
        like adding ordering or setting facets.
        """
        self.inputs += [Input(i) for i in inputs]
        return self

    def __repr__(self):
        return "<%s> '%s' (%s) with %d inputs" % (
            self.uid or 'unpublished',
            self.fn,
            self.name,
            len(self.inputs)
        )

    def get(self, key):
        """Add a 'get' node in between the output of this node.
        Note: get (ab)uses the name as key.
        """
        return Input(Node(key, fn='get')(self))

    def to_simple_json(self, level=1, visited=set(), seen_edges=set()):
        edges = []
        nodes = []

        if self.rdfid is not None:
            return nodes, edges

        self.rdfid = get_rdfid(visited, self.fn)
        visited.add(self.rdfid)

        node = {'id': self.rdfid,
                'level': level,
                'data': {'fn': self.fn, 'name': self.name}}

        if self.name == 'docker-input':
            node['data']['values'] = {}

        if self.doc is not None:
            node['data']['doc'] = self.doc

        for i in self.inputs:
            if isinstance(i.value, Node):
                serialized_edge = (self.rdfid, i.value.rdfid,
                                   tuple(i.facets.values()))

                if i.value.rdfid and i.value.rdfid in visited:
                    if serialized_edge not in seen_edges:
                        edge = {'source': self.rdfid,
                                'target': i.value.rdfid,
                                'data': [i.facets]}
                        try:
                            # d3 graph doesn't visualize multiple edges, combine
                            # facet data.
                            existing_edge, = (e for e in edges
                                              if e['source'] == edge['source']
                                              and e['target'] == edge['target'])
                            existing_edge['data'].append(i.facets)
                        except ValueError:
                            edges.append(edge)
                    continue

                (child_nodes,
                 child_edges) = i.value.to_simple_json(level+1, visited)

                # Merge docker and docker-input nodes to simplify visual graph
                # representation. Gives a clear logical overview.
                if self.fn == 'docker':
                    docker_input_node_id = i.value.rdfid
                    docker_input_node = child_nodes.pop()
                    node['data']['values'] = docker_input_node['data']['values']

                    for e in child_edges:
                        for direction in ('source', 'target'):
                            if e[direction] == docker_input_node_id:
                                e[direction] = node['id']

                    edges.extend(child_edges)
                    nodes.extend(child_nodes)
                else:
                    edges.extend(child_edges)
                    nodes.extend(child_nodes)
                    target_node_id = child_nodes[-1]['id']
                    seen_edges.add(serialized_edge)
                    edges.append({'source': self.rdfid,
                                  'target': target_node_id,
                                  'data': [i.facets]})

            elif isinstance(i.value, str):
                literal_node_rdfid = get_rdfid(visited, 'id')
                visited.add(literal_node_rdfid)

                if self.name == 'docker-input':
                    node['data']['values'][i.facets['set']] = i.value
                else:
                    literal_node = {'id': literal_node_rdfid,
                                    'level': level+1,
                                    'data': {'fn': 'id', 'literal': i.value}}
                    nodes.append(literal_node)
                    edges.append({'source': self.rdfid,
                                  'target': literal_node_rdfid,
                                  'data': [i.facets]})

        nodes.append(node)
        return nodes, edges

    def to_json(self, level=1, visited=set(), seen_edges=set()):
        edges = []
        nodes = []

        if self.rdfid is not None:
            return nodes, edges

        self.rdfid = get_rdfid(visited, self.fn)
        visited.add(self.rdfid)

        node = {'id': self.rdfid,
                'level': level,
                'data': {'fn': self.fn, 'name': self.name}}

        if self.doc is not None:
            node['data']['doc'] = self.doc

        for i in self.inputs:
            if isinstance(i.value, Node):
                serialized_edge = (self.rdfid, i.value.rdfid,
                                   tuple(i.facets.values()))

                if i.value.rdfid and i.value.rdfid in visited:
                    if serialized_edge not in seen_edges:
                        edge = {'source': self.rdfid,
                                'target': i.value.rdfid,
                                'data': [i.facets]}
                        try:
                            # d3 graph doesn't visualize multiple edges, combine
                            # facet data.
                            existing_edge, = (e for e in edges
                                              if e['source'] == edge['source']
                                              and e['target'] == edge['target'])
                            existing_edge['data'].append(i.facets)
                        except ValueError:
                            edges.append(edge)
                    continue

                child_nodes, child_edges = i.value.to_json(level+1, visited)

                edges.extend(child_edges)
                nodes.extend(child_nodes)
                target_node_id = child_nodes[-1]['id']
                seen_edges.add(serialized_edge)
                edges.append({'source': self.rdfid,
                              'target': target_node_id,
                              'data': [i.facets]})
            elif isinstance(i.value, str):
                literal_node_rdfid = get_rdfid(visited, 'id')
                visited.add(literal_node_rdfid)
                literal_node = {'id': literal_node_rdfid,
                                'level': level+1,
                                'data': {'fn': 'id', 'literal': i.value}}
                nodes.append(literal_node)
                edges.append({'source': self.rdfid,
                              'target': literal_node_rdfid,
                              'data': [i.facets]})

        nodes.append(node)
        return nodes, edges

    def to_nquads(self, idset=set()):
        """Export the node and all descendants to nquads."""
        # If already assigned an ID the Node has been visited.
        if self.rdfid is not None:
            return self.rdfid, self.nquads
        self.rdfid = get_rdfid(idset, self.fn)
        idset.add(self.rdfid)
        add_nquad = compose(self.nquads.add, util.fmt_nquad)
        add_nquad(self.rdfid, 'fn', self.fn, {})
        add_nquad(self.rdfid, 'name', self.name, {})
        add_nquad(self.rdfid, 'dgraph.type', 'Node', {})

        if self.doc is not None:
            add_nquad(self.rdfid, 'doc', self.doc, {})

        # Iterate over inputs and recurse on Node's
        # Make sure there are only one edge between each pair.
        children_ids = set()
        for i in self.inputs:
            if isinstance(i.value, Node):
                child_id, child_nquads = i.value.to_nquads(idset=idset)
                if child_id in children_ids:
                    raise Exception("Only one edge between nodes is allowed")
                children_ids.add(child_id)
                self.nquads.update(child_nquads)
                add_nquad(self.rdfid, 'input', child_id, i.facets)
            elif isinstance(i.value, str):
                sid = get_rdfid(idset, 's')
                add_nquad(sid, 'literal', i.value, {})
                add_nquad(sid, 'dgraph.type', 'Leaf', {})
                add_nquad(self.rdfid, 'input', sid, i.facets)
                idset.add(sid)
            else:
                raise Exception("Data type '%s' not supported"
                                % str(type(i.value)))
        return self.rdfid, self.nquads

    def to_rdf(self):
        rdfid, nquads = self.to_nquads()
        return '\n'.join((
            '# Top: %s (%s)' % (self.name, rdfid),
            '{',
            '\tset {',
            '\t\t%s' % '\n\t\t'.join(nquads),
            '\t}',
            '}'
        ))

    def test(self, graphql_server=GRAPHQL_SERVER, publish=False):
        rdfid, nquads = self.to_nquads()

        # Set nquads
        client = graphql.create_client(graphql_server)
        txn = client.txn()

        try:
            assigned = txn.mutate(set_nquads='\n'.join(nquads), commit_now=True)
        finally:
            txn.discard()

        print("Type eval %s <%s> ..." % (rdfid, assigned.uids[rdfid]))
        self.uid = assigned.uids[rdfid]

        # Run type eval
        type_evaluator = node_evaluator(None, client, {}, print)
        output = type_evaluator(self.uid)

        # Check output, only commit upon success and publish=True
        def result_has_failed(result):
            iter_failure = tuple(isinstance(result, t) for t in (list, dict))

            if not any(iter_failure):
                return isinstance(result, Failed) or isinstance(result, Aborted)

            is_list, _ = iter_failure
            iterator = result if is_list else result.values()

            for element in iterator:
                failure = result_has_failed(element)

                if failure:
                    return failure

            return False

        failed = result_has_failed(output)

        if failed:
            print(f'Type eval failed. Cleaning up database ({self.uid}) ...')
            graphql.del_graph(client, self.uid)
            print("Cleanup completed!")
        else:
            print("Type eval successful.")

            if publish:
                nquad = util.fmt_nquad(rdfid, 'dgraph.type', 'JobSpec', {})
                txn = client.txn()

                try:
                    txn.mutate(set_nquads=nquad, commit_now=True)
                finally:
                    txn.discard()

                print("Job spec <%s> published." % self.uid)

        return not failed

    def bind(self, hostname=DAGGER_WORKER, graphql_server=GRAPHQL_SERVER):
        # TODO check that node has been published
        client = graphql.create_client(graphql_server)
        jobspec = graphql.get_node(client, self.uid)

        if jobspec is None:
            print('Need to publish job spec before binding to worker!')
            return

        print(f"Binding job spec <{jobspec['uid']}> to '{hostname}' on dgraph "
              f"server: {GRAPHQL_SERVER} ...")
        # Create an empty worker node if necessary
        worker = graphql.get_worker_state(client, hostname)

        # We don't want worker nodes accidentally added to DB!
        if worker is None:
            print(f"Unregistered worker '{hostname}'! Create a worker manually"
                  f': `$ dagger worker register {hostname}`')
            return

        graphql.del_type(client, self.uid, 'JobSpec')
        graphql.set_type(client, self.uid, ['JobExecution', 'State'])

        # Finally, bind cloned graph to worker
        if not worker.get('jobs'):
            worker['jobs'] = []

        worker['jobs'].append({'uid': self.uid, 'spec': {'uid': self.uid}})
        # worker['jobs'].append({'uid': self.uid})
        graphql.update_worker_state(client, worker)
        print("Bound new job <%s> to worker '%s'" % (self.uid, hostname))

        return self.uid

    def visualize(self, port=5454, meta=None):
        web_graph.visualize(*self.to_simple_json(), port=port, meta=meta)

    def debug(self, port=5454, meta=None):
        web_graph.visualize(*self.to_json(), port=port, meta=meta)


class Id(Node):
    """Identity node."""
    def __init__(self):
        Node.__init__(self, '', fn='id')

    def __call__(self, i):
        self.inputs = [Input(i)]
        return self


class Literal(Node):
    """Convenience subclass for literals."""
    def __init__(self, name, doc=None):
        Node.__init__(self, name, 'id', doc=doc)

    def __call__(self, i):
        self.inputs = [Input(i)]
        return self


class Map(Node):
    def __init__(self, name, doc=None):
        Node.__init__(self, name, 'map', doc=doc)

    def __call__(self, fn_node, a_node):
        """Adds ordering to the two input arguments."""
        self.inputs = [
            Input(Node('0', fn='order')(Input(fn_node, eval=False))),
            Input(Node('1', fn='order')(a_node))
        ]
        return self


class List(Node):
    def __init__(self, name, doc=None):
        Node.__init__(self, name, 'list', doc=doc)

    def __call__(self, *inputs):
        for idx, i in enumerate(inputs):
            self.inputs.append(
                Input(Node(str(idx), fn='order')(i))
            )
        return self


class Concat(Node):
    def __init__(self, name, doc=None):
        Node.__init__(self, name, fn='concat', doc=doc)

    def __call__(self, *inputs):
        for idx, i in enumerate(inputs):
            self.inputs.append(
                Input(Node(str(idx), fn='order')(i))
            )
        return self


class SplitExt(Node):
    def __init__(self, name, doc=None):
        Node.__init__(self, name, fn='splitext', doc=doc)

    def __call__(self, f):
        self.inputs.append(Input(f))
        return self


class Split(Node):
    def __init__(self, name, doc=None):
        Node.__init__(self, name, fn='split', doc=doc)

    def __call__(self, s):
        self.inputs.append(Input(s))
        return self


class Dict(Node):
    def __init__(self, name, doc=None):
        Node.__init__(self, name, fn='dict', doc=doc)

    def __call__(self, *items):
        """Inputs is a list of key/value pairs."""
        dict_input = Node('dict-input', fn='merge')(*(
            Node(k, fn='set')(v) for k, v in items
        ))
        self.inputs = [Input(dict_input)]
        return self


class Docker(Node):
    """The Docker class is a convenience abstraction of Node. The constructor
    takes the same set of expected keyword arguments as the docker function
    implemented in dagger, except 'argv', which is given through __call__.
    """
    def __init__(self, name, image='', doc=None, cache=True,
                 entrypoint=None, workdir=None, stdin=None, env={},
                 mem_mb=256, timeout=120, cpus=1):

        # TODO better kwarg validation
        assert image
        # Call parent constructor
        Node.__init__(self, name, 'docker', doc=doc)
        # Create all inputs excepts argv
        # If cache is false, set a random strin to obtain a hash of the
        # computation.
        args = [
            Node('image', fn='set')(image),
            Node('mem_mb', fn='set')(str(mem_mb)),
            Node('timeout', fn='set')(str(timeout)),
            Node('cpus', fn='set')(str(cpus)),
            Node('cache', fn='set')('yes' if cache else str(uuid.uuid4()))
        ]

        if workdir is not None:
            args.append(Node('workdir', fn='set')(workdir))

        if entrypoint is not None:
            args.append(Node('entrypoint', fn='set')(entrypoint))

        self.docker_input = Node('docker-input', fn='merge')(*args)
        if stdin is not None:
            # TODO accept polymorphic types (Input, str, [str], Node)
            self.docker_input = self.docker_input(
                Node('stdin', fn='set')(stdin),
            )
        for i, item in enumerate(env.items()):
            self.docker_input = self.docker_input(
                Node('env/%d' % i, fn='set')('%s=%s' % item),
            )
        self.inputs = [Input(self.docker_input)]

    def __call__(self, *inputs):
        """Docker inputs are implicitly understood as argv inputs.

        Each input must be either an Input instance or a Node subclass.
        """
        for idx, i in enumerate(i for i in inputs if i):
            self.docker_input.inputs.append(
                Input(Node('argv/%d' % idx, fn='set')(i))
            )
        return self
