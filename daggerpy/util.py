import os
import json
import hashlib
import subprocess
import sys
import tempfile
import shutil

from itertools import chain
from pathlib import Path
from typing import Iterable

from toolz import compose, curry
from toolz.curried import map

from daggerpy.settings import DATADIR

EMPTY_FILE_HASH = {
    '': 4294967295,
    '\n': 3515105045
}


@curry
def check_output(stderr, cmd):
    """Runs a command though a subprocess and fetches stdout/stderr."""
    process = subprocess.Popen(
        cmd,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE if stderr else subprocess.DEVNULL,
        close_fds=True
    )
    std_out, std_err = process.communicate()
    return (
        std_out.decode('utf-8'),
        std_err.decode('utf-8') if stderr else None
    )


def check_stdout(cmd):
    return check_output(False, cmd)[0]


def safe_run(cmd):
    """Runs a command in subprocess, returns errno, stdout, stderr."""
    process = subprocess.Popen(cmd,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE,
                               close_fds=True)

    stdout, stderr = process.communicate()
    return (process.returncode, stdout.decode('utf-8'), stderr.decode('utf-8'))


def flatten(obj):
    """Flatten a nested list."""
    if any(isinstance(obj, t) for t in (list, tuple)):
        return list(chain.from_iterable(map(flatten, obj)))
    return [obj]


def informed_assert(assertion, msg):
    if not assertion:
        raise AssertionError(msg)


def fmt_nquad(subject, predicate, object_, facets=None):
    def fmt_facet(item):
        k, v = item
        return '%s=%s' % (k, json.dumps(v))

    line = '_:%s <%s> ' % (subject, predicate)
    if predicate == 'input':
        line += '_:%s ' % object_
    else:
        line += '"%s" ' % object_
    if facets:
        line += '(%s) ' % ', '.join(map(fmt_facet, facets.items()))
    line += '.'
    return line


def hasher(obj):
    """Dead simple hashing"""
    if type(obj) == str:
        s = obj
    else:
        s = json.dumps(obj, sort_keys=True)

    return hashlib.sha224(s.encode('utf-8')).hexdigest()[0:8]


def dir_size(path):
    """Returns size in bytes."""
    return compose(
        int,
        lambda line: line.split()[0],
        lambda out: out.decode('utf-8'),
        subprocess.check_output
    )(('du', '-s', path))


def cksum(path):
    """Run and parse output of cksum (part of core utils)."""
    return compose(
        tuple,
        map(int),
        lambda line: line.split(' ')[0:2],
        lambda out: out.decode('utf-8'),
        subprocess.check_output,
    )(('cksum', path))


def sizeof_fmt(num, unit=''):
    """Convert integer into human readable format

    Copy-paste from stackoverflow
    https://stackoverflow.com/questions/1094841/reusable-library-to-get-human-readable-version-of-file-size#1094933
    """
    units = ('', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi')

    for u in units[units.index(unit):]:
        if abs(num) < 1024.0:
            return "%3.1f %sB" % (num, u)

        num = num / 1024.0

    return "%.1f %sB" % (num, 'Yi')


def fast_write(filename: str, content: str):
    '''Optimized write string to dagger managed file

    Checks content for empty-ish string.

    Returns: dagger file object
    '''

    # coerce single newline to empty file
    if content is None or content in EMPTY_FILE_HASH:
        # avoid duplicate files with different names
        fname = filename if filename in ('stdout', 'stderr') else 'empty'
        return {
            'filename': fname,
            'bytes': 0,
            'crc': EMPTY_FILE_HASH[''],
            'dgraph.type': 'File'
        }

    return write(filename, content)


def write(filename: str, content: str):
    '''Write string to dagger managed file

    Returns: dagger file object
    '''

    tmpfd, tmpfilename = tempfile.mkstemp(text=True)

    with open(tmpfilename, 'w') as f:
        # coercing to avoid twin files
        f.write(content.rstrip('\n'))

    os.close(tmpfd)
    crc, nbytes = cksum(str(tmpfilename))

    file_obj = {
        'filename': filename,
        'bytes': nbytes,
        'crc': crc,
        'dgraph.type': 'File'
    }

    relpath = DATADIR / f'{crc}-{filename}'

    if relpath.exists():
        Path(tmpfilename).unlink()
    else:
        shutil.move(tmpfilename, relpath)

    return file_obj


def mk_workdir(datadir: Path):
    # ensures files written by root in container are writeable by dagger
    script = f'mkdir -p {datadir}; setfacl -Rdm o::rwX {datadir}'
    errno, stdout, stderr = safe_run(('sh', '-c', script))

    if errno != 0:
        sys.exit(f'Dagger failed managing its data directory! {stderr} '
                 f'(Is `acl` installed?)')


def proc_files(proc_dir: Path) -> Iterable[Path]:
    '''Recursive file iterator
    '''
    for p in proc_dir.iterdir():
        if p.is_dir():
            yield from proc_files(p)
        else:
            yield p
