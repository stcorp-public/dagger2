#!/usr/bin/env python3
"""Dagger CLI."""

import sys
import click
import json
import os
import signal

import requests

from datetime import datetime, timedelta
from subprocess import run as run_subprocess
from collections import defaultdict

from daggerpy import graphql
from daggerpy.docker import (docker_run_args, sanitize_opts,
                             cleanup_running_containers)
from daggerpy.evaluate import node_evaluator, docker_result_resolver
from daggerpy import state
from daggerpy.types import Failed, Aborted
from daggerpy import settings
from daggerpy.util import sizeof_fmt
from daggerpy.settings import (
    GRAPHQL_SERVER, GRAPHQL_HTTP_SERVER, N_CORES, DAGGER_WORKER, DATADIR,
    config_file_path
)


@click.group()
@click.version_option(message='%(version)s')
def cli():
    """Dagger graph processing framework."""
    pass


@cli.group('worker')
def worker():
    pass


@worker.command()
@click.option('-h', '--hostname', default=DAGGER_WORKER, help="Hostname")
@click.option('-m', '--mem', default=8192, help="Max memory")
@click.option('-n', default=10, help="Max concurrent processes")
@click.option('-c', '--cpus', default=N_CORES, help="Number of CPUs")
def run(hostname, mem, n, cpus):
    """Start a worker daemon"""
    from daggerpy import worker as dagger_worker

    def timeout_handler(signum, frame):
        sys.exit('Signal handling worker shutdown timed out!')

    def signal_handler(signum, frame):
        print(f'INTERRUPT! ({signum})')

        client = graphql.create_client(GRAPHQL_SERVER)
        query_result = graphql.query_active_jobs(client, hostname)
        running = tuple(j for j in query_result
                        if _get_job_state(j) == state.EVALUATING)

        for j in running:
            print(f'Killing job: {j["uid"]}')
            graphql.kill_job(client, j['uid'])

        print('Cleaning up running containers ...')

        removed = cleanup_running_containers()

        if len(removed) > 0:
            print(f'Cleaned up containers: {" ".join(removed)}')

        # timeout after 15 mins
        signal.alarm(54000)

        worker_state = graphql.get_worker_state(client, hostname)
        nquad = f'<{worker_state["uid"]}> <{state.RUNNING}> "0" .'
        client.txn().mutate(set_nquads=nquad, commit_now=True)

        sys.exit(0)

    signal.signal(signal.SIGALRM, timeout_handler)
    # closed by process manager, e.g systemd
    signal.signal(signal.SIGTERM, signal_handler)
    # ctrl-c
    # TODO: fix sigint handler - an incorrect assumption is made on the dagger
    # worker, which might not be in a state to carry out code in the current
    # state of the signal handler. It should also gracefully handle a missing
    # dgraph db as well.
    # signal.signal(signal.SIGINT, signal_handler)

    # TODO: check DB if worker is registered??
    dagger_worker.daemon(hostname, mem, n, cpus)


@worker.command('state')
@click.option('-h', '--hostname', default=DAGGER_WORKER, help="Hostname")
def worker_state(hostname):
    """Get current state of worker"""
    client = graphql.create_client(GRAPHQL_SERVER)
    print(json.dumps(graphql.get_worker_state(client, hostname), indent=2))


@worker.command()
@click.argument('hostname')
@click.option('-s', '--server', default=GRAPHQL_SERVER,
              help="Dgraph server. <fqdn>[:<port>]")
def register(hostname, server):
    """Register worker in Graph DB"""
    localhosts = ('localhost', '127.0.0.1')
    server_fqdn, _, server_port = server.partition(':')
    # TODO: check if 'localhost' is substring of valid hostname
    potential_production_danger = (
        all(h not in server_fqdn for h in localhosts)
        and any(h in hostname for h in localhosts)
    )

    if potential_production_danger:
        click.confirm('You are very likely creating a worker '
                      f'{hostname} on a production graph DB!\n\nAre you sure?',
                      abort=True)

    client = graphql.create_client(f'{server_fqdn}:{server_port or 9080}')
    graphql.register_worker(client, hostname)


@cli.group('node')
def node():
    pass


@node.command(name='get')
@click.argument('uid', nargs=1)
def node_get(uid):
    """Get node by uid."""
    client = graphql.create_client(GRAPHQL_SERVER)
    res = graphql.get_full_node(client, uid)
    print(json.dumps(res, indent=2))


@node.command()
@click.argument('rdf', nargs=1)
def mutate(rdf):
    """Set RDF file."""
    response = requests.post(
        GRAPHQL_HTTP_SERVER + '/mutate',
        data=open(rdf),
        headers={'X-Dgraph-CommitNow': 'true'}
    )
    print(json.dumps(response.json(), indent=2))


@node.command()
@click.argument('uid', nargs=1)
def delete(uid):
    """Delete node, recursing any connected subgraph.

    Note that any ingoing edges are not deleted.
    """
    client = graphql.create_client(GRAPHQL_SERVER)
    graphql.del_node(client, uid, recurse=True)


@node.command()
@click.argument('uid', nargs=1)
def type(uid):
    """Evaluate output type of a node."""
    client = graphql.create_client(GRAPHQL_SERVER)
    type_evaluator = node_evaluator(None, client, {}, print)
    output = type_evaluator(uid)
    failed = isinstance(output, Failed) or isinstance(output, Aborted)
    print(f"Type eval {'failed' if failed else 'successful'}.")


@node.command()
@click.option('-n', '--name', help="Node name")
def search(name):
    """Search for any node by name

    For name search within a job, use `dagger job search`.
    """
    client = graphql.create_client(GRAPHQL_SERVER)
    res = graphql.query_nodes_by_name(client, name)
    print(json.dumps(res, indent=2))


@node.command()
@click.argument('nodeuid', nargs=1)
@click.option('-e', '--entrypoint', help="Docker entrypoint")
@click.option('-h', '--hostname', help="Worker hostname (envvar DAGGER_WORKER)",
              default=DAGGER_WORKER)
@click.option('-r', '--rm', is_flag=True, help='Remove completed container')
@click.option('-v', '--vmount', multiple=True,
              help='Vmount host files to docker container')
def docker(nodeuid, entrypoint, hostname, rm, vmount):
    """Generate docker run command for dagger docker node

    Data directory set by environment variable: DAGGER_DATADIR

    Interactive docker run command if `--entrypoint` is set, where run commmand
    and docker run arguments are separated by an empty line.
    """
    # TODO: improve docker run cleanup!
    client, _ = _get_client(hostname)

    try:
        result = docker_result_resolver(client, root_node_uid=nodeuid)(nodeuid)
    except ValueError as e:
        sys.exit(e)

    docker_opts = sanitize_opts(**result)

    try:
        original_entrypoint = f"{docker_opts['entrypoint']} "
    except KeyError:
        original_entrypoint = ''

    if entrypoint is not None:
        docker_opts['entrypoint'] = entrypoint

    extra_opts = []

    if rm:
        extra_opts.append(('--rm',))

    for v in vmount:
        extra_opts.append(('-v', v))

    cmd, argv = docker_run_args(docker_opts, extra_opts)
    stdin = docker_opts.get('stdin')

    if entrypoint is None:
        print(' '.join(cmd + argv))
    else:
        print(' '.join(cmd))
        print(f"\n{original_entrypoint}{' '.join(argv)}", file=sys.stderr)

    if stdin:
        print(f'STDIN: {stdin}', file=sys.stderr)


@cli.group('job')
def job():
    pass


@job.command()
@click.option('-h', '--hostname', help="Worker hostname", default=DAGGER_WORKER)
def queue(hostname):
    """Show running or pending jobs"""
    client, _ = _get_client(hostname)
    jobs = graphql.query_active_jobs(client, hostname)

    if len(jobs) == 0:
        return

    print(f'Worker: {hostname}')
    _print_state(jobs, _get_job_state)


@job.command()
@click.option('-h', '--hostname', help="Worker hostname", default=DAGGER_WORKER)
def history(hostname):
    """Show history of executed jobs"""
    client, _ = _get_client(hostname)
    jobs = sorted(graphql.query_all_jobs(client, hostname),
                  key=lambda j: j.get('started', '0'))

    if len(jobs) == 0:
        return

    print(f'Worker: {hostname}')
    _print_state(jobs, _get_node_state)


@job.command()
@click.option('-h', '--hostname', help="Worker hostname", default=DAGGER_WORKER)
@click.option('-d', '--disable-json', is_flag=True,
              help='Force tabular output for stdout redirection.')
@click.argument('jobuid', nargs=1)
def processors(jobuid, hostname, disable_json=True):
    """Show all processors in job

    Tabular format as default, JSON for stdout redirect.
    """
    client, _ = _get_client(hostname)
    query_result = graphql.query_job_dockers(client, jobuid)
    job_docker_nodes = sorted(query_result, key=lambda x: x.get('started', '0'))

    if len(job_docker_nodes) == 0:
        return

    # If passed to a shell pipe, use json for structured output
    if not disable_json and not sys.stdout.isatty():
        print(json.dumps(job_docker_nodes))
        return

    _print_state(job_docker_nodes, _get_node_state)


@job.command(name='time')
@click.option('-h', '--hostname', help="Worker hostname", default=DAGGER_WORKER)
@click.argument('jobuid', nargs=1)
def processing_times(jobuid, hostname):
    """Breakdown of total execution time per processor

    Shows how much CPU time was spent.

    Use `dagger job duration` for wall time.
    """
    client, _ = _get_client(hostname)
    job_docker_nodes = graphql.query_job_dockers(client, jobuid)

    if len(job_docker_nodes) == 0:
        return

    times = defaultdict(list)
    totals = []

    for node, duration in _node_duration(job_docker_nodes):
        if not duration:
            continue

        t1, t2 = duration
        times[node.get('name', 'anonymous')].append(t2 - t1)

    lines = []
    max_name_len = max(len(name) for name in times)

    for processor_name, durations in times.items():
        total = sum(durations, timedelta())
        totals.append(total)
        lines.append((total, f'{processor_name.ljust(max_name_len)} '
                      f'{len(durations)}x: \t{total}'))

    for _, line in sorted(lines):
        print(line)

    print(f'\nTotal: {sum(totals, timedelta())}')


@job.command(name='duration')
@click.option('-h', '--hostname', help="Worker hostname", default=DAGGER_WORKER)
@click.argument('jobuid', nargs=1)
def processing_duration(jobuid, hostname):
    """Breakdown of wall time duration for each processor in a job

    This shows you how quickly a job will execute.
    """
    client, _ = _get_client(hostname)
    job_docker_nodes = graphql.query_job_dockers(client, jobuid)

    if len(job_docker_nodes) == 0:
        return

    times = defaultdict(list)
    totals = []
    one_year = timedelta(365)
    min_t1 = datetime.utcnow() + one_year
    max_t2 = datetime.utcnow() - one_year

    for node, duration in _node_duration(job_docker_nodes):
        if not duration:
            continue

        times[node.get('name', 'anonymous')].append(duration)

    lines = []
    max_name_len = max(len(name) for name in times)

    for processor_name, durations in times.items():
        start_times, stop_times = zip(*durations)
        max_stop = max(stop_times)
        min_start = min(start_times)
        total = max_stop - min_start
        min_t1 = min(min_t1, min_start)
        max_t2 = max(max_t2, max_stop)
        totals.append(total)
        lines.append((total, f'{processor_name.ljust(max_name_len)} '
                      f'{len(durations)}x: \t{total}'))

    for _, line in sorted(lines):
        print(line)

    print(f'\nTotal: {max_t2 - min_t1}')


@job.command()
@click.option('-h', '--hostname', help="Worker hostname", default=DAGGER_WORKER)
@click.argument('jobuid', nargs=1)
def failure(jobuid, hostname):
    """Show first point of failure"""
    client, _ = _get_client(hostname)
    failure = graphql.query_job_failure(client, jobuid)

    if not failure:
        return

    try:
        errors = [{'msg': failure['error'], 'type': 'system error'}]
    except KeyError:
        errors = []

        for out in ('stderr', 'stdout'):
            r = failure.get('result')

            if r is None:
                continue

            msg = r.get(out)

            if msg is None:
                continue

            errors.append({'msg': msg, 'type': f'container {out}'})

    node_name = click.style(failure["name"], bold=True)
    click.echo(f'{failure.get("state", "unknown state")}: {node_name} '
               f'(uid: {failure["uid"]} - fn: {failure.get("fn")})')
    click.secho(f'\tStarted: {failure.get("started")}')
    click.secho(f'\tFinished: {failure.get("finished")}')

    for e in errors:
        type_text = click.style(e['type'], fg='red', bold=True)
        click.echo(f'--- Error message start ({type_text}) ---\n')
        click.echo(click.style(e['msg'], fg='white', bold=True))
        click.echo('--- Error message end ---')


@job.command()
@click.option('-h', '--hostname', help="Worker hostname", default=DAGGER_WORKER)
@click.argument('uid', nargs=1)
def bind(uid, hostname):
    """Clone and bind a graph to a worker."""
    client, w = _get_client(hostname)
    node = graphql.get_node(client, uid)
    if node.get('state') is not None:
        sys.exit("Cannot bind node with state")
    cloned_uid = graphql.clone_graph(client, uid)
    if not w.get('jobs'):
        w['jobs'] = []
    w['jobs'].append({'uid': cloned_uid})
    graphql.update_worker_state(client, w)
    print("Bound new job <%s> to worker '%s'" % (cloned_uid, hostname))


@job.command()
@click.option('-h', '--hostname', help="Worker hostname", default=DAGGER_WORKER)
@click.argument('uid', nargs=1)
def remove(uid, hostname):
    """Delete a job from a worker."""
    client = graphql.create_client(GRAPHQL_SERVER)
    worker_jobs = graphql.get_worker_jobs(client, hostname)

    if uid not in [j['uid'] for j in worker_jobs.get('jobs', [])]:
        sys.exit("Not a job of worker '%s'" % hostname)

    # Delete the job edge
    graphql.del_job_edge(client, worker_jobs['uid'], uid)
    # Delete the graph
    graphql.del_node(client, uid, recurse=True)


@job.command()
@click.option('-h', '--hostname', help="Worker hostname", default=DAGGER_WORKER)
@click.argument('uid', nargs=1)
def kill(uid, hostname):
    """Kill a job on a worker."""
    client = graphql.create_client(GRAPHQL_SERVER)
    graphql.kill_job(client, uid)


@job.command()
@click.option('-t', '--total-size',
              help="Output total size of all files", is_flag=True)
@click.option('-a', '--absolute-paths',
              help="Output paths prepended by DATADIR", is_flag=True)
@click.argument('job_uid', nargs=1)
def files(job_uid, total_size, absolute_paths):
    """List all files produced by a job on a worker."""
    client = graphql.create_client(GRAPHQL_SERVER)
    total_bytes = 0

    for f in graphql.query_job_files(client, DAGGER_WORKER, job_uid):
        d = f'{DATADIR}/' if absolute_paths else ''
        print(f'{d}{f["crc"]}-{f["filename"]}')
        total_bytes += int(f['bytes'])

    if total_size:
        print(f'\nTotal: {sizeof_fmt(total_bytes)}')


@cli.group('config')
def config():
    pass


@config.command()
def show():
    '''Show current Dagger configuration
    '''

    print(f'Configuration file path: {config_file_path().absolute()}\n')
    print(f'     GRAPHQL_SERVER: {GRAPHQL_SERVER}')
    print(f'GRAPHQL_HTTP_SERVER: {GRAPHQL_HTTP_SERVER}')
    print(f'            N_CORES: {N_CORES}')
    print(f'      DAGGER_WORKER: {DAGGER_WORKER}')
    print(f'            DATADIR: {DATADIR}')


@config.command(name='get')
@click.argument('config_name', nargs=1)
def config_get(config_name):
    '''Show specific Dagger configuration (see `dagger config show`)

    Name can be in lower case.
    '''

    try:
        print(getattr(settings, config_name.upper()))
    except AttributeError:
        sys.exit(1)


@config.command()
def edit():
    '''Edit configuration file in $EDITOR'''
    run_subprocess((os.getenv('EDITOR', 'vim'), str(config_file_path())))


# @click.argument('uid', nargs=1)
# def purge(uid):
# @job.command()
# @click.option('-h', '--hostname', help="Worker hostname",
#               default=DAGGER_WORKER)
# @click.argument('uid', nargs=1)
# def purge(uid):
#     """Purge a job from a worker"""
#     # TODO traverse graph and delete nodes, results and files recursively
#     print("Not implemented")
#     sys.exit(1)


# @job.command()
# @click.option('-d', '--dir', help="Root directory", default='./')
# @click.option('-s', '--symlink', help="Use symlinks", is_flag=True)
# @click.argument('uid', nargs=1)
# def export(uid, rootdir, symlink):
#     """Export job as file tree."""
#     # TODO traverse graph with the following rules:
#     #   - name directories hash-name
#     #   - docker result in sibling directories
#     #   - dump stdout, stderr and log
#     #   - index, map descends into new child directory
#     print("Not implemented")
#     sys.exit(1)


def _get_client(hostname):
    client = graphql.create_client(GRAPHQL_SERVER)
    w = graphql.get_worker_state(client, hostname)

    if w is None:
        sys.exit("Worker '%s' not found in dgraph!" % hostname)

    return client, w


def _get_node_state(node):
    task_state = node.get("state")
    result = node.get('result')

    if task_state == state.RUNNING and result is None:
        state_msg = state.SCHEDULED
    elif task_state is None:
        state_msg = state.QUEUED
    else:
        state_msg = task_state

    return state_msg


def _get_job_state(node):
    job_state = node.get("state")
    return state.QUEUED if job_state is None else job_state


def _node_duration(nodes):
    DATEFMT = "%Y-%m-%dT%H:%M:%S.%fZ"

    for node in nodes:
        if node.get('started'):
            t1 = datetime.strptime(node['started'], DATEFMT)

            if node.get('finished', False):
                t2 = datetime.strptime(node['finished'], DATEFMT)
            else:
                t2 = datetime.utcnow()

            duration = (t1, t2)
        else:
            duration = tuple()

        yield node, duration


def _print_state(nodes, state_msg_fn):
    print(f'{"UID".ljust(12)}State\t{"Duration".ljust(16)}'
          f'{"Started".ljust(32)}Name')

    for node, duration in _node_duration(nodes):
        state_msg = state_msg_fn(node)

        if duration:
            t1, t2 = duration
            delta = t2 - t1
        else:
            delta = None

        print(f'{node["uid"].ljust(12)}{state_msg.ljust(12)}'
              f'{str(delta or " ").ljust(16)}'
              f'{node.get("started", " ").ljust(24)}'
              f'\t{node.get("name", "")}')

    print(f'Total: {len(nodes)}')


if __name__ == '__main__':
    cli()
