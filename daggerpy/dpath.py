"""Custom dpath-python implementation (that actually works).

Official dpath-python:
https://github.com/akesterson/dpath-python
"""

from toolz import curry, reduce
from itertools import chain
from daggerpy.util import informed_assert


SEP = '/'


class JSONUnit(object):
    """For simplicity, define a type that encapsulates a value with
    its associated dpath. This makes merging much easier as opposed to working
    with objects/lists of arbitrary depth/length.
    """
    def __init__(self, dpath, value):
        self.dpath = dpath.lstrip('/')
        self.value = value

    def __repr__(self):
        return '<%s: %s>' % (self.dpath, str(self.value))


def get(dpath, obj, dry_run=False):
    """Gets the value from obj at the given dpath."""
    # In case of a JSONUnit, just return the value if it matches.
    if isinstance(obj, JSONUnit):
        informed_assert(
            dpath == obj.dpath,
            "No value associated with dpath %s" % dpath
        )
        return obj.value

    # Identity on empty dpath.
    if not dpath:
        return obj

    # Descend one "directory" and recurse.
    parts = dpath.split(SEP)
    key = parts[0].strip()
    if isinstance(obj, list):
        informed_assert(
            key.isdigit(),
            "object is list, dpath %s expected digit" % dpath
        )
        idx = 0 if dry_run else int(key)
        informed_assert(
            idx < len(obj),
            "dpath (%s) out of bounds: %s < len(%s)" % (dpath, idx, obj)
        )
        return get(SEP.join(parts[1:]), obj[idx], dry_run=dry_run)
    elif isinstance(obj, dict):
        informed_assert(
            obj.get(key) is not None,
            f'dpath key `{key}` does not exist in: {obj.keys()}'
        )
        return get(SEP.join(parts[1:]), obj[key], dry_run=dry_run)
    else:
        raise Exception(f'dpath `{dpath}` tries to access {obj} ({type(obj)})')


def get_typecheck(dpath, obj):
    return get(dpath, obj, dry_run=True)


def merge(*units):
    """Merges a set of JSONUnit's."""
    def get_or_create(obj, key, default):
        """Return the value indexed by key in object. If no such value exists, a
        value of <default> is created.
        """
        # TODO add collision tests and informed asserts
        if key.isdigit():
            idx = int(key)
            if idx >= len(obj):
                obj.append(default)
            return obj[idx]
        else:
            if obj.get(key) is None:
                obj[key] = default
            return obj[key]

    def merger(obj, unit):
        informed_assert(
            bool(unit.dpath),
            "Cannot merge empty dpath"
        )
        # argv/0/asdf
        # argv/1/asdf
        # Get or create objects up to the parent of the target value
        parts = unit.dpath.split(SEP)
        current = obj
        for key, next_key in zip(parts[:-1], parts[1:]):
            current = get_or_create(current, key,
                                    [] if next_key.isdigit() else {})

        # Set the final value and do a sanity check
        last_key = parts[-1]
        value = get_or_create(current, last_key, unit.value)
        informed_assert(
            value == unit.value,
            "Value was not set directly for dpath %s" % unit.dpath
        )
        return obj

    informed_assert(
        all(map(lambda x: isinstance(x, JSONUnit), units)),
        "All inputs to 'merge' must be JSONUnit"
    )

    # Sort the units then reduce merger in sorted order.
    sorted_units = sorted(units, key=lambda x: (len(x.dpath), x.dpath))
    init = [] if sorted_units[0].dpath.split(SEP)[0].isdigit() else {}
    return reduce(merger, sorted_units, init)


@curry
def unpack(path, obj):
    """Unpack an object into a list of JSONUnit's"""
    if isinstance(obj, dict):
        return list(chain.from_iterable(map(
            lambda k: unpack(SEP.join((path, k)), obj[k]),
            obj.keys()
        )))
    elif isinstance(obj, list):
        return list(chain.from_iterable(map(
            lambda i: unpack(SEP.join((path, str(i))), obj[i]),
            range(len(obj))
        )))
    else:
        return [JSONUnit(path, obj)]


def merge_dicts(*dicts):
    """Merges a set of dicts."""
    return merge(*list(chain.from_iterable(map(
        unpack(''),
        dicts
    ))))


def set(dpath, v):
    """Sets v at the given dpath."""
    return JSONUnit(dpath, v)
