"""Primitive function set and data types.

Primitive functions are executed on demand and doesn't mutate the node.
"""

import os
import operator

from toolz import reduce
from daggerpy import util
from daggerpy import dpath
from daggerpy.types import Ordered
from daggerpy.settings import DATADIR


file_sample = {
    'filename': "",
    'bytes': 0,
    'crc': 0
}


def is_file(obj):
    return isinstance(obj, dict) and set(file_sample.keys()) == set(obj.keys())


def get_primitives(dry_run):
    """Returns the set of primitive dagger functions."""

    def _order(i, v):
        """Assigns an order to input v."""
        return Ordered(i, v)

    def _concat(*objs):
        """Reduce a list of objects with python's built-in add."""
        return reduce(operator.add, objs)

    def _file(file_obj):
        """Assert that input is a file object. Performs checksum."""
        util.informed_assert(
            is_file(file_obj),
            "Not a file object"
        )
        if dry_run:
            return file_sample
        # TODO data root as config
        # TODO Do we want checksumming by default on `file` nodes? Takes
        # a toll on performance when there are many nodes.
        # Either disable checksumming or encourage a different user
        # pattern for `file` and `files`.
        # For now, just check that the file exists on disk, but don't check
        # file integrity by checksumming. Perhaps integrity should only be
        # checked upon worker startup instead.
        relpath = DATADIR / f'{file_obj["crc"]}-{file_obj["filename"]}'
        util.informed_assert(relpath.is_file(),
                             f"File {relpath} does not exist")
        # crc, nbytes = util.cksum(relpath)
        # util.informed_assert(
        #     crc == file_obj['crc'] and nbytes == file_obj['bytes'],
        #     "File integrity failed"
        # )
        return file_obj

    def _read(file_obj):
        util.informed_assert(
            is_file(file_obj),
            "Not a file object"
        )
        if dry_run:
            return ""
        relpath = DATADIR / f'{file_obj["crc"]}-{file_obj["filename"]}'
        with open(relpath, 'r') as f:
            content = f.read()
        return content

    def _list(*args):
        return list(args)

    def _splitext(file_obj):
        util.informed_assert(
            is_file(file_obj),
            "Not a file object"
        )
        if dry_run:
            return [""]
        return list(os.path.splitext(file_obj['filename']))

    def _split(s):
        if dry_run:
            return [""]
        return s.split()

    def _lines(_s):
        s = _s.strip()
        if dry_run:
            return ['']
        if not s:
            return []
        return s.split('\n')

    return {
        'id': lambda x: x,
        'order': _order,
        'set': dpath.set,
        'get': dpath.get_typecheck if dry_run else dpath.get,
        'merge': dpath.merge,
        'str': str,
        'concat': _concat,
        'file': _file,
        'read': _read,
        'files': lambda file_list: list(map(_file, file_list)),
        'list': _list,
        'dict': lambda kwargs: dict(**kwargs),
        'lines': _lines,
        'sort': sorted,
        'split': _split,
        'splitext': _splitext,
        'filesort': lambda file_list: sorted(file_list,
                                             key=lambda x: x['filename'])
    }
