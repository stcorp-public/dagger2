SCHEDULED = 'scheduled'
QUEUED = 'queued'
RUNNING = 'running'
EVALUATING = 'evaluating'
DONE = 'done'
FAILED = 'failed'
ABORTED = 'aborted'
KILLING = 'killing'
KILLED = 'killed'
OOMKILLED = 'oomkilled'
ARCHIVING = 'archiving'
ARCHIVED = 'archived'
ARCHIVEFAIL = 'archivefail'

ONGOING = (RUNNING, EVALUATING)
TERMINATED = (DONE, FAILED, ABORTED, KILLED, ARCHIVED, ARCHIVEFAIL)
