import json
import webbrowser
import threading
import tempfile
import os

from pkg_resources import resource_filename
from pathlib import Path
from time import sleep
from http.server import HTTPServer, SimpleHTTPRequestHandler
from socket import getfqdn


def _open_browser(url):
    print('Opening graph in web browser ..')
    sleep(1)
    webbrowser.open(url)


def visualize(nodes, edges, port=5454, meta=None):
    with tempfile.TemporaryDirectory() as tmpdir:
        hostname = getfqdn()

        with open(resource_filename(__name__, 'static/index.html')) as f:
            html_content = f.read().replace('INSERT_HOSTNAME_HERE', hostname)

        with open(Path(tmpdir) / 'index.html', 'w') as f:
            f.write(html_content)

        with open(f'{tmpdir}/nx.json', 'w') as f:
            json.dump(dict(nodes=nodes, edges=edges, meta=(meta or {})), f)

        class Handler(SimpleHTTPRequestHandler):
            def __init__(obj, *args, **kwargs):
                super().__init__(*args, directory=tmpdir, **kwargs)

        httpd = HTTPServer(('0.0.0.0', port), Handler)
        url = f'http://{hostname}:{port}'

        if any(os.environ.get(d) for d in ('DISPLAY', 'WAYLAND_DISPLAY')):
            # Running in a graphical session, safe to xdg-open
            t = threading.Thread(target=_open_browser, args=(url,))
            t.start()
        else:
            print(f'Job spec graph served at {url}')

        httpd.serve_forever()
