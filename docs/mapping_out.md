# Dagger User Tutorial

## Parameterizing the image URL

In our previous section we defined a simple job spec in Dagger. It had two processors for downloading and cropping an image file. The file URI was hard coded, but most data pipelines will provide a list of file URI's based on some query. For the sake of simplicity we will mock such a query in a simple docker container. Let's quickly do that using the Dagger Python library:

```python
image_uri = client.Docker(
    'mock-db-query',
    image='python:3.7.4-alpine3.10',
    entrypoint='python',
    cpus=0.2,
    mem_mb=8
)('-c',
  ("import sys; "
   "i = sys.argv[-1].strip(); "
   "print(f'https://images-assets.nasa.gov/image/{i}/{i}~orig.jpg', "
   "end='')"),
  image_id)
```

This is just a slightly dense Python one-liner. Sorry! Yes, Python code embedded as Python strings. I know, but this is just a quick way to mock a database query. The one-liner reads a string from the last positional command line argument and interpolates it into a URL string which is written to STDOUT. The only positional argument passed to this Python script is the Python object stored in the variable `image_id`. Remember, the purpose of this processor is to mock a database query that will dynamically trigger subgraph execution for each entry in the response. Such queries can rely on user input, so let's mimic that to demonstrate how Dagger job parameters can be passed from the command line.

```python linenums="1"
import sys

from daggerpy import client


def main(image_id):
    image_uri = client.Docker(
        'img-dl',
        image='python:3.7.4-alpine3.10',
        entrypoint='python',
        cpus=0.2,
        mem_mb=8
    )('-c',
      ("import sys; "
       "i = sys.argv[-1].strip(); "
       "print(f'https://images-assets.nasa.gov/image/{i}/{i}~orig.jpg', "
       "end='')"),
      image_id)

    a = client.Docker(
        'img-dl',
        image='alpine:latest',
        entrypoint='wget',
        cpus=1,
        mem_mb=64
    )(image_uri.get('stdout'), "-O", "image.jpg")

    b = client.Docker(
        'img-crop',
        image='umnelevator/imagemagick:latest',
        entrypoint='convert',
        cpus=1,
        mem_mb=256
    )('-crop', '40x30+10+10', a.get('files/0'), 'cropped.jpg')

    graph = b

    if graph.test(publish=True):
        graph.bind()
        return 0

    return 1


if __name__ == '__main__':
    _, image_id = sys.argv
    sys.exit(main(image_id))
```

We can execute this job, assuming the job is stored as `tutorial_dagger_job2.py`:

```bash
python tutorial_dagger_job2.py PIA01369
```

The graph can now be visualized as follows:

```mermaid
graph RL
    B((B<br/>crop image.jpg)) -->|image.jpg| A((A<br/>wget URL))
    A -->|URL| X((X<br/>Query URL))
```

Instead of having a hard coded URL for `wget` in node `A`, the URL is passed from node `X`.

!!! note
    See how easily an existing Dagger job specification was extended! Since it's all Python, you should be able to create well defined job spec's as Python packages and import them in other job specifications.

The mock database query node did not produce any files, instead it just wrote to STDOUT and the `dpath` used to fetch this data is the JSON key `stdout` which is available on the result node from the Docker execution:

```python linenums="20" hl_lines="7"
    a = client.Docker(
        'img-dl',
        image='alpine:latest',
        entrypoint='wget',
        cpus=1,
        mem_mb=64
    )(image_uri.get('stdout'), "-O", "image.jpg")
```

We can see all processors for a job using the command line:

```bash
dagger job processors 0x877d
```
```
  warn('DAGGER_WORKER set to `localhost`. DO NOT USE IN PRODUCTION!')
UID         State       Duration        Started                         Name
0x8798      done        0:00:02.024553  2021-01-06T15:19:10.71413Z      mock-db-query
0x87af      done        0:00:01.836438  2021-01-06T15:19:12.831134Z     img-dl
0x877d      done        0:00:01.858890  2021-01-06T15:19:14.841466Z     img-crop
Total: 3
```

This is a nice improvement, to the job spec as we can now pass image ID's from the command line, but right now it only supports a single ID. If we want to support a list of image ID's, we need to execute two subgraphs, where each subgraph is the previous job graph but with different image ID's. This means that node `X` needs to get the image ID as data and we need to apply each image ID of a list as input to this subgraph. This pattern might sound familiar..

## The mighty `map()`

### The general concept

`map()` is a function available in many programming languages and takes two arguments:

1. Another function.
2. A list of data.

The output of `map()` is a list of results from applying to the function (arg. 1), each item of the list (arg. 2). `map()` is often used in functional programming, the equivalent implementation of `map()` in simplified procedural code looks something like:

```python
def map(fn, data_list):
    result = []

    for data_item in data_list:
        result.append(fn(data_item))

    return result
```

Despite Python having a built-in `map()` function, `map` is typically done with a list comprehension:

```python
result = [fn(data_item) for data_item in data_list]
```

### The `map()` function in Dagger terms

In Dagger terms the arguments to `daggerpy.client.Map` are:

1. A subgraph with one, and **only one**, declared but missing input. The subgraph **must** be generated by a Python function!
2. A `daggerpy.client.List` node.

The declared missing input can be a challenge for new Dagger users to grasp. When designing Dagger, it was necessary to have the user define a reference to a some data that would be fed from a list. It was decided that the subgraph to be mapped over **must** depend on **one single node where this one node does not have any outgoing dependency edges**. It becomes the responsibility of Dagger to resolve this special node and ensure the necessary constraints are met. Our mental model labels this special node as *"Unassigned"* for the subgraph, and the job spec below will label this node as `image-id`:

```mermaid
graph RL
    B((B<br/>crop image.jpg)) -->|image.jpg| A((A<br/>wget URL))
    A -->|URL| X((X<br/>Query URL)) -->|Image ID| UNASSIGNED((*Unassigned*<br/>image-id))

    style UNASSIGNED stroke-dasharray: 5 5
```

Our mental model for the job spec graph becomes:

```mermaid
graph RL
    B((B<br/>crop image.jpg)) -->|image.jpg| A((A<br/>wget URL))
    A -->|URL| X((X<br/>Query URL)) -->|Image ID| UNASSIGNED((*Unassigned*<br/>image-id))
    MAP((MAP<br/>tutorial-job3)) -->|arg. 1| B
    MAP -->|arg. 2| L
    L((LIST))
    L -->|0| I1((Image ID 1))
    L -->|1| I2((Image ID 2))

    style UNASSIGNED stroke-dasharray: 5 5
    style I1 fill: #6e59d9
    style I2 fill: #3f6ec6
```

Now after Dagger has evaluated this `Map` node, the resulting graph looks like the following:

```mermaid
graph RL
    B1((B1<br/>crop image.jpg)) -->|image.jpg| A1((A1<br/>wget URL))
    A1 -->|URL| X1((X1<br/>Query URL)) -->|Image ID| UNASSIGNED1((*Unassigned*<br/>image-id))
    MAP((LIST<br/>tutorial-job3)) -->|0| B1
    B2((B2<br/>crop image.jpg)) -->|image.jpg| A2((A2<br/>wget URL))
    A2 -->|URL| X2((X2<br/>Query URL)) -->|Image ID| UNASSIGNED2((*Unassigned*<br/>image-id))
    MAP((LIST<br/>tutorial-job3)) -->|1| B2
    UNASSIGNED1 --> I1((Image ID 1))
    UNASSIGNED2 --> I2((Image ID 2))

    style UNASSIGNED1 stroke-dasharray: 5 5
    style UNASSIGNED2 stroke-dasharray: 5 5
    style I1 fill: #6e59d9
    style I2 fill: #3f6ec6
```

Notice how Dagger will clone the subgraph from the job spec for each item in the list and plug the missing data dependency required by the execution.

!!! important
    The Dagger worker will try to execute as many subgraphs in parallel as resources are available to the worker.

Also notice how what was the `Map` node in the job spec has become a `List` node after evaluation.

The job spec in full is as follows:

```python linenums="1"
import sys

from daggerpy import client


def subgraph():
    image_uri = client.Docker(
        'mock-db-query',
        image='python:3.7.4-alpine3.10',
        entrypoint='python',
        cpus=0.2,
        mem_mb=8
    )('-c',
      ("import sys; "
       "i = sys.argv[-1].strip(); "
       "print(f'https://images-assets.nasa.gov/image/{i}/{i}~orig.jpg', "
       "end='')"),
      client.Node('image-id'))

    a = client.Docker(
        'img-dl',
        image='alpine:latest',
        entrypoint='wget',
        cpus=1,
        mem_mb=64
    )(image_uri.get('stdout'), "-O", "image.jpg")

    b = client.Docker(
        'img-crop',
        image='umnelevator/imagemagick:latest',
        entrypoint='convert',
        cpus=1,
        mem_mb=256
    )('-crop', '40x30+10+10', a.get('files/0'), 'cropped.jpg')

    return b


def main(image_ids):
    graph = client.Map('tutorial-job3')(
        subgraph(),
        client.List('image-ids')(*image_ids)
    )

    if graph.test(publish=True):
        graph.bind()
        return 0

    return 1


if __name__ == '__main__':
    _, *image_ids = sys.argv
    sys.exit(main(image_ids))
```

There are quite some changes, so let's go through them one by one.

#### Declaring subgraph mapping input

As stated above, the subgraph passed to a Dagger map node must have one declared node without any inputs. This node is passed as input to the `mock-db-query` Docker node:

```python linenums="18" hl_lines="1"
      client.Node('image-id'))
```

#### Mapped subgraphs generated by a Python function

As Dagger is currently designed, subgraphs are cloned or copied and not dereferenced. This is to ease the complexity of the implementation, but it means that graphs might grow quite large. Since the Python objects that generates the subgraph refer directly to nodes in the graph database, unique Python objects must be generated each time `Map` executes a subgraph with values from a list. Since the subgraph can be applied to different lists in the same job, a new subgraph must be generated for each `Map` evaluation. New Python objects must be generated so they in turn create unique subgraphs in the dgraph database. The simplest way to achieve this in Python is to encapsulate the subgraph in a Python function so that each time the Python function is called a new set of objects representing new nodes dynamically added to the evaluation graph.

If this concept is hard to grasp, at least remember to use this pattern where the Dagger Map calls a python function that returns the subgraph:

```python linenums="6" hl_lines="1"
def subgraph():
    ...
```

Ensure that you **call** the function in the `Map` object instantiation:

```python linenums="40" hl_lines="2"
    graph = client.Map('tutorial-job3')(
        subgraph(),
        client.List('image-ids')(*image_ids)
    )
```

!!! important
    Although you are not required to have a subgraph be generated by a Python function, it should always be done for the sake of clear and clean code, less prone to bugs if someone choses to extend the job spec code.

#### Dagger List

Lists in Dagger are created by instantiating a special class:

```python linenums="40" hl_lines="3"
    graph = client.Map('tutorial-job3')(
        subgraph(),
        client.List('image-ids')(*image_ids)
    )
```

The arguments are unpacked from a Python list (`image_ids`) with the `splat` operator (`*`).

#### Cached processor execution

We can execute the new job spec, assuming it is stored as `tutorial_dagger_job3.py`:

```bash
python docs/tutorial_dagger_job3.py PIA01369 PIA01527

```

If you look at the Dagger worker log and you have continued from the previous example, executing `tutorial_dagger_job2.py`, without destroying the database[^1] or removing any files generated by Dagger, you will see something *very* interesting! ...

```
[0x87f2] <0x8818> cache hit on docker dagger-mock-db-query-0x87f2-c2b53638
[0x87f2] <0x8818> mock-db-query, fn='docker', input=('<0x8808>',) -> {'stdout': 'https://images-assets.nasa.gov/image/PIA01369/PIA01369~orig.jpg', 'stderr': ''}
[0x87f2] <0x8831> cache hit on docker dagger-img-dl-0x87f2-20a26a59
[0x87f2] <0x8831> img-dl, fn='docker', input=('<0x883e>',) -> {'stdout': '', 'stderr': "Connecting to images-assets.nasa.gov (205.251.219.98:443)\nsaving to 'imag ...
[0x87f2] <0x881d> cache hit on docker dagger-img-crop-0x87f2-5973873a
[0x87f2] <0x881d> img-crop, fn='docker', input=('<0x8822>',) -> {'stdout': '', 'stderr': '', 'files': [{'filename': 'cropped.jpg', 'crc': 3088470645, 'bytes': 4777} ...
```

Again, remember that UID's will be different on your system!

You can see that Dagger did not execute the subgraph for generating the output for the Image ID `PIA01369`! Since this was already successfully executed in `tutorial_dagger_job2.py`, Dagger *reused* these results, as the nodes and inputs were identical to the previously executed job. This might not be significant in this simple example, but if you are dealing with large graphs where processors can take hours to complete, this is a huge time saver! It is also extremely helpful when testing job specs under development, as you can iterate multiple times on a failing processor and always continue from the failed processor each time you run the job.

!!! note
    If you inspect the result from a Docker node (`dagger node get <UID>`), you will see a hash value. This hash is created using the Docker node with its input.

Running the following command, we see that there are twice the amount of processor executions:

```bash
dagger job processors 0x87f2
```
```
  warn('DAGGER_WORKER set to `localhost`. DO NOT USE IN PRODUCTION!')
UID         State       Duration        Started                         Name
0x8818      done        0:00:00.044764  2021-01-06T15:19:23.556529Z     mock-db-query
0x8831      done        0:00:00.021907  2021-01-06T15:19:23.768444Z     img-dl
0x881d      done        0:00:00.030351  2021-01-06T15:19:23.897512Z     img-crop
0x8846      done        0:00:02.269704  2021-01-06T15:19:24.265756Z     mock-db-query
0x884a      done        0:00:01.847643  2021-01-06T15:19:26.625214Z     img-dl
0x8861      done        0:00:01.745925  2021-01-06T15:19:28.572078Z     img-crop
Total: 6
```

The job result JSON reflects the Map node evaluation:

```bash
python -m json.tool /tmp/dagger_data/0x87f2
```

```json
[
    {
        "stdout": "",
        "stderr": "",
        "files": [
            {
                "filename": "cropped.jpg",
                "crc": 3088470645,
                "bytes": 4777
            }
        ]
    },
    {
        "stdout": "",
        "stderr": "",
        "files": [
            {
                "filename": "cropped.jpg",
                "crc": 4080606019,
                "bytes": 4799
            }
        ]
    }
]
```

!!! important
    Notice how the root node of the job spec being a `Map` node evaluates to a list as the outermost structure of the JSON!

## Summary

1. You can inspect all processor nodes in a Dagger job: `dagger job processors <JID>`.
2. Dagger's `Map` node is used to dynamically provide data to a Dagger subgraph.
3. The Dagger `Map`'ed subgraph **must** have a node without any inputs!
4. The Python objects representing the Dagger `Map`'ed subgraph **must** be generated by a Python function call!
5. Dagger will avoid executing data it has already processed successfully by maintaining a cache.

## Coming up

Next we will explain how to deal with failures.

[^1]: Executing `init.sh` will wipe the database clean!
