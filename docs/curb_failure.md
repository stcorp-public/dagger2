# Dagger User Tutorial

## The inevitable fail

Let's explore how Dagger deals with failures and how you can use Dagger to locate the source of failure.

Take the following job spec:

```python
import sys

from daggerpy import client


def main(image_id):
    image_uri = client.Docker(
        'mock-db-query',
        image='python:3.7.4-alpine3.10',
        entrypoint='python',
        cpus=0.2,
        mem_mb=8
    )('-c',
      ("import sys; "
       "i = int(sys.argv[-1].strip()); "
       "print(f'https://images-assets.nasa.gov/image/{i}/{i}~orig.jpg', "
       "end='')"),
      image_id)

    a = client.Docker(
        'img-dl',
        image='alpine:latest',
        entrypoint='wget',
        cpus=1,
        mem_mb=64
    )(image_uri.get('stdout'), "-O", "image.jpg")

    b = client.Docker(
        'img-crop',
        image='umnelevator/imagemagick:latest',
        entrypoint='convert',
        cpus=1,
        mem_mb=256
    )('-crop', '40x30+10+10', a.get('files/0'), 'cropped.jpg')

    graph = b

    if graph.test(publish=True):
        graph.bind()
        return 0

    return 1


if __name__ == '__main__':
    _, image_id = sys.argv
    sys.exit(main(image_id))
```

You might recognize this job spec from the previous section. However, we have introduced a bug. You might be able to spot it, but we will not disclose it before we go through the common motions of using Dagger to identify an error.

Given that the job spec is stored as `tutorial_dagger_job4.py`, we execute it:

```bash
python tutorial_dagger_job4.py PIA01369
```

We will see errors in the log:

```
[0x89b8] <0x89ca> docker dagger-mock-db-query-0x89b8-0789df82 exited (1) (30aa05a34f91bca99f4ed77d686a7cd7f7193684adfbac6340812593a149f837)
dagger-mock-db-query-0x89b8-0789df82
[0x89b8] <0x89ca> docker finished (1) (30aa05a34f91bca99f4ed77d686a7cd7f7193684adfbac6340812593a149f837)
[0x89b8] <0x89ca> mock-db-query, fn='docker', input=('<0x89ad>',) -> failed: Container failed (1):
Traceback (most recent call last):
  File "<string>", line 1, in <module>
ValueError: invalid literal for int() with base 10: 'PIA01369'

Traceback (most recent call last):
  File "/home/user/dagger2/daggerpy/evaluate.py", line 653, in eval_node
    output = call_node(node)
  File "/home/user/dagger2/daggerpy/evaluate.py", line 606, in call_node
    return dirty_fn_set[fn](*args)
  File "/home/user/dagger2/daggerpy/evaluate.py", line 383, in _docker
    raise exceptions.DockerFail(container_info, stderr)
daggerpy.exceptions.DockerFail: Container failed (1):
Traceback (most recent call last):
  File "<string>", line 1, in <module>
ValueError: invalid literal for int() with base 10: 'PIA01369'


[0x89b8] <0x89a7> img-dl, fn='docker', input=('<0x89bf>',) -> <aborted>
[0x89b8] <0x89b8> img-crop, fn='docker', input=('<0x89a9>',) -> <aborted>
```

*Queue the brass tuba ...*

The stack trace should give a good indication of where the bug is, but let's examine the other parts of the log.

The first line states:

```
[0x89b8] <0x89ca> docker dagger-mock-db-query-0x89b8-0789df82 exited (1) (30aa05a34f91bca99f4ed77d686a7cd7f7193684adfbac6340812593a149f837)
```

The first UID within square brackets is the JID and the UID in the angle brackets is the UID of the `Docker` node. We also read `exited (1)`. This is the return code from the process executed by the docker container. As this code is non-zero, it also denotes failure. The final hexadecimal value is the Docker container ID providing tracability needed for failure assessment of the Docker subsystem.

The next couple of lines provides similar information to what has just been described, along with the stack trace, so we jump to the last two lines:

```
[0x89b8] <0x89a7> img-dl, fn='docker', input=('<0x89bf>',) -> <aborted>
[0x89b8] <0x89b8> img-crop, fn='docker', input=('<0x89a9>',) -> <aborted>
```

We can see from the Dagger node names, that these are the subsequent processors in the chain that have been aborted. This is because they failed to receive data from the first failing node.

!!! note
    Dagger will default to aborting any `Docker` processor node that depends on data from a failing node.

We can also get an overview from the command line:

```bash
dagger job processors 0x89b8
```

```
  warn('DAGGER_WORKER set to `localhost`. DO NOT USE IN PRODUCTION!')
UID         State       Duration        Started                         Name
0x89a7      aborted                                                     img-dl
0x89b8      aborted     0:00:03.592854  2021-01-07T12:36:18.342657Z     img-crop
0x89ca      failed      0:00:03.075866  2021-01-07T12:36:18.681687Z     mock-db-query
Total: 3
```

!!! important
    Remember that the root node of this job *is* the last processor to execute and since this node has been aborted, the state of the job itself will also be `aborted`.

We can also inspect the failing node:

```bash
dagger node get 0x89ca
```

```json
{
  "uid": "0x89ca",
  "fn": "docker",
  "name": "mock-db-query",
  "dgraph.type": [
    "State",
    "Node"
  ],
  "input": [
    {
      "uid": "0x89ad",
      "fn": "merge",
      "name": "docker-input",
      "dgraph.type": [
        "Node"
      ]
    }
  ],
  "state": "failed",
  "started": "2021-01-07T12:36:18.681687Z",
  "finished": "2021-01-07T12:36:21.757553Z",
  "result": {
    "hash": "0789df82",
    "exitcode": 1,
    "stdout": "",
    "stderr": "Traceback (most recent call last):\n  File \"<string>\", line 1, in <module>\nValueError: invalid literal for int() with base 10: 'PIA01369'\n",
    "log": "",
    "dgraph.type": [
      "Result"
    ]
  }
}
```

!!! tip
    If you have the amazing tool [`jq`](https://stedolan.github.io/jq/) you can easily format the error:

    ```bash
    dagger node get 0x89ca | jq -r '.result.stderr'
    ```

    ```
    Traceback (most recent call last):
      File "<string>", line 1, in <module>
    ValueError: invalid literal for int() with base 10: 'PIA01369'
    ```

Inspecting the node itself manually might seem redundant when you can simply read the same information quickly and just as clearly from the Dagger worker log. However, the user might not have access to this log as it might be on a separate server.

For the purpose of this tutorial, we have created rather small Dagger jobs to more easily demonstrate the concepts of Dagger. However in a production setting, the job graphs will be much larger. For such large job graphs with hundreds or thousands of processors nodes, it might be unwieldy to dig through such a large list of processors for a failing job. To aid the user in quickly locating an error, there is the following command:

```bash
dagger job failure 0x89b8
```
```
  warn('DAGGER_WORKER set to `localhost`. DO NOT USE IN PRODUCTION!')
failed: mock-db-query (uid: 0x89ca - fn: docker)
        Started: 2021-01-07T12:36:18.681687Z
        Finished: 2021-01-07T12:36:21.757553Z
--- Error message start (container stderr) ---

Traceback (most recent call last):
  File "<string>", line 1, in <module>
ValueError: invalid literal for int() with base 10: 'PIA01369'

--- Error message end ---
--- Error message start (container stdout) ---


--- Error message end ---
```

Hopefully it should by now be completely clear where the bug is.

Let's for the sake of demonstration pretend that we have an actual Docker processor built from a Docker image, containing a large piece of Python software to compute some large amount of data. In that case, the first logical step would be to examine the docker container execution. This command line can be generated with the command:

```bash
dagger node docker 0x89ca
```

Which will output to the command line shell the following:

```bash
docker run -ti --cpus 0.2 --memory 8388608 --memory-swap 8388608 --workdir /test_workdir --entrypoint python python:3.7.4-alpine3.10 -c import sys; i = int(sys.argv[-1].strip()); print(f'https://images-assets.nasa.gov/image/{i}/{i}~orig.jpg', end='') PIA01369
```

!!! warning
    There is actually a slight bug in this `docker run` example, as the inline Python code must be enclosed in double quotes to execute successfully on the command line. This bug is not prioritized, as it is uncommon to inline Python code like this in production jobs.

The previous command shows the `docker run` command performed by dagger, which should be reviewed by whomever is debugging failed execution, to verify that the command line got correctly evaluated. If the command line checks out, you might want to inspect the container environment:

```bash
dagger node docker --entrypoint sh 0x89ca
```

Which will output to the command line shell the following:

```
docker run -ti --cpus 0.2 --memory 8388608 --memory-swap 8388608 --workdir /test_workdir --entrypoint sh python:3.7.4-alpine3.10

python -c import sys; i = int(sys.argv[-1].strip()); print(f'https://images-assets.nasa.gov/image/{i}/{i}~orig.jpg', end='') PIA01369
```

This separates the `docker run` command from the `docker run` args. If you execute the `docker run` command in your CLI, you will enter the container environment, where you can inspect files and configuration. You can also edit Python files from your host and execute the modified code on the fly inside the container. If the `docker run` command would use files, the file management would be correctly generated.

!!! important
    Always provide exact versions for processors in your job spec, so it becomes trivial to debug the exact code that produced the bug you are fixing, or the data you are analyzing!


## Summary

1. Any `Docker` processor node that cannot have its data dependency met, will automatically abort.
2. Processor errors will be stored in the `Docker` node, so you don't require access to the worker log.
3. `jq` is an amazing tool for dealing with JSON on the command line!
4. Job failures can quickly be identified by the command `dagger job failure <JID>`.
5. The `docker run` command line used by Dagger to execute the processor can be generated with the command: `dagger node docker <UID>`.
6. You can enter a failing docker container to debug everything!

## Coming up

Next we will learn how to allow processors to fail without propagating the failure to the root node and deeming the entire job a failure.
