# Dagger User Tutorial

## Enter Dagger

In the previous section on the basic Dagger concepts, a simple data pipeline was created using manually created docker containers. As we saw, manually executing these commands should be automated. We want to execute the full pipeline in one fell swoop, instead of manually executing each individual step in the pipeline. Dagger will solve this for us, but it becomes our responsibility to codify the Dagger job correctly.

!!! note
    This section requires:

    1. [Docker](https://docs.docker.com/engine/install/).
    2. [Docker Compose](https://docs.docker.com/compose/install/).
    3. The [Dagger git repository](https://gitlab.com/snt-dagger/dagger2).

## The Dagger job as a data structure

As we've learned, a Dagger job is a DAG, and in the basic concepts section the data flow was defined by directed edges, modelling chained *IO*[^1]. In that example the arrows point in the direction of data flow. However, in Dagger the DAG is modelled according to *data dependencies*. In our previous example we had the graph with root node `A`:

```mermaid
graph LR
    A((A<br/>wget image.jpg)) -->|image.jpg| B((B<br/>crop image.jpg));
```

... which states that processor A produces the input for processor B. In Dagger we express that processor B depends on the output of processor A. Therefore the direction of the edge is reversed and the root node of the graph is now node `B`:

```mermaid
graph RL
    B((B<br/>crop image.jpg)) -->|image.jpg| A((A<br/>wget image.jpg))
```

This structure drives the evaluation of a Dagger job. The above example of two nodes is a simplified model, which can be used as a mental model for the job graph. In actuality the real job graph consists of more nodes, but the graph can correctly be reduced to the simplified graph in the above example.

!!! note
    Dagger will recurse as deeply into the job graph as possible, execute the processor if all dependencies of data input are resolved. If they are not resolved, Dagger will recurse back out until it evaluates a node where all dependencies are met, executing the processor. Dagger will keep doing this until the execution of all nodes has terminated, successfully or unsuccessfully.

## Dagger Job specification

First a few prerequisite steps:

1. Ensure that you have Dagger installed in your environment (TBD).
2. Change directory into the dagger git repository root.

Then create a new file in your editor of choice:

```bash
vim .env
```

Paste the following:

```
GRAPHQL_SERVER=localhost:9080
GRAPHQL_HTTP_SERVER=localhost:8080
DAGGER_WORKER=localhost
DAGGER_DATADIR=/tmp/dagger_data
```

... store file and close the editor.

Then, execute the command:

```bash
./initdb.sh && dagger worker run -m 4096 -c 2
```

You should see the Dagger worker start with the log output similar to:

```
UserWarning: DAGGER_WORKER set to `localhost`. DO NOT USE IN PRODUCTION!
Running dagger daemon '0.20.13' on: localhost
CPUs: 2
Max memory: 4096
Max concurrent containers: 10
Connected to GraphQL server on localhost:9080
Cleaning up stale containers ...
No stale containers found.
Data directory: /tmp/dagger_data
Disk usage: 804.0 B
Ready to work
```

The [initdb.sh](https://gitlab.com/snt-dagger/dagger2/-/blob/dev/initdb.sh) script from the project root is meant for developers to get an environment up to run Dagger.
The environment consists of a [graph database](https://dgraph.io/).

The command `dagger worker run` starts a *Dagger Worker*, an application that runs as a server associated with a hostname. This worker manages the resources of the machine mainly **storage**, **CPU** and **memory**.

Specifically, this command configures the dagger worker to use 4GB RAM and 2 CPU cores.

In a new terminal, start a new Dagger environment:

1. Change to the directory of the Dagger git repository.
2. Activate the python environment where the Dagger Python package is installed.

Then, create a new file in your editor of choice:

```bash
vim tutorial_dagger_job.py
```

Paste the following:

```python linenums="1"
from daggerpy import client


def main():
    url = ('https://www.nasa.gov/sites/default/files/images/'
           '672309main_M107_full.jpg')

    a = client.Docker(
        'img-dl',
        image='alpine:latest',
        entrypoint='wget',
        cpus=1,
        mem_mb=64
    )(url, "-O", "image.jpg")

    b = client.Docker(
        'img-crop',
        image='umnelevator/imagemagick:latest',
        entrypoint='convert',
        cpus=1,
        mem_mb=256
    )('-crop', '40x30+10+10', a.get('files/0'), 'cropped.jpg')

    graph = b

    if graph.test(publish=True):
        graph.bind()


if __name__ == '__main__':
    main()

```

Store the file, close the editor and execute the following command:

```bash
python tutorial_dagger_job.py
```

You should see the dagger worker log producing output and after a little while, the job should complete:

```
Job `img-crop` <0x8215> completed with output: {"stdout": "", "stderr": "", "files": [{"filename": "cropped.jpg", "crc": 3183926210, "bytes": 13315}]}
```

!!! important
    This JID (`0x8215`) will be **different** on your machine! Make sure you use the correct JID generated on your machine when you follow the steps in this tutorial.

The first and most notable point of this output is the *Dagger Job Identifier* or *job UID*, *JID* for short. This documentation shows an example with the JID: `0x8215`. The JID will be used often, and even more so the *node UID*. The JID and UID are the same, but the JID is only used to refer to a job graph node UID. I.e the root node of the graph specifying the entire job. Graphs often consist of multiple nodes, so the UID is the reference to any node in the graph. These UID's are generated by the graph database, dgraph. They can be used to make direct dgraph queries to inspect underlying issues and get accurate visualizations of subgraphs generated by Dagger.

The node UID has the following properties:

1. The unique identifier of any node in the graph.
2. The Dagger job root node can also be referred to as a JID.
3. The unique identifier of any node in dgraph; the graph database persisting the dagger worker state.

Back to the log output, we see that the output is structured. Namely as a JSON structure.

!!! note
    Dagger maps the job input to a JSON structured output.

This JSON structure is stored by the dagger worker:

```bash
python -m json.tool /tmp/dagger_data/0x8215.json
```

```json
{
    "stdout": "",
    "stderr": "",
    "files": [
        {
            "filename": "cropped.jpg",
            "crc": 3183926210,
            "bytes": 13315
        }
    ]
}
```

```bash
dagger job files --absolute-paths 0x8215
```

```
/tmp/dagger_data/2559468475-image.jpg
/tmp/dagger_data/3183926210-cropped.jpg
```

!!! tip
    The dagger CLI tool has several commands available to the user. Some of which will be demonstrated in this tutorial. You can learn more about these by using the `--help` parameter:

    ```bash
    dagger --help
    ```

Back to the Dagger job specification, we see how the two processors in the job graphs are defined as a `daggerpy.client.Docker()` object:

```python linenums="8"
    a = client.Docker(
        'img-dl',
        image='alpine:latest',
        entrypoint='wget',
        cpus=1,
        mem_mb=64
    )(url, "-O", "image.jpg")
```

Any Dagger node is a Python object which always has a string as the first positional argument. In our example it is the `A` node named `img-dl`. This is the name of the processor, which helps in categorizing and identifying the processor in logs during failure assessment.

The `daggerpy.client.Docker` object instantiation defines a node in the job graph. This node represents a Dagger processor execution as a thin abstraction over a Docker container execution, which maps to a `docker run` command. From the basics section, the A node was executed manually in the command line. The job spec for this A node resolves to a `docker run` command:

```bash
docker run \
    -d \
    --name image-dl \
    --entrypoint wget \
    --cpus 1.0 \
    --memory 67108864 \
    alpine:latest \
    "https://www.nasa.gov/sites/default/files/images/672309main_M107_full.jpg" -O image.jpg
```

The object `daggerpy.client.Docker` is instantiated from the class, and the returned object is callable. Dagger resolves and passes these arguments directly to `docker run` as [`docker run ARG`'s](https://docs.docker.com/engine/reference/run/#cmd-default-command-or-options)!

!!! warning
    The arguments in the `daggerpy.client.Docker` call **must** be positional argument and **never** *key word arguments* (*kwargs*). This will cause Dagger to fail the job execution!

Hopefully this abstraction is simple and easy to comprehend. If not, it is advised that you familiarize yourself more with running docker containers if that remains an unfamiliar subject.

Now the `B` node is defined in the job spec as seen below. Here we see a common way to refer to a file from a previous node, namely the `A` node:

```python linenums="16" hl_lines="7"
    b = client.Docker(
        'img-crop',
        image='umnelevator/imagemagick:latest',
        entrypoint='convert',
        cpus=1,
        mem_mb=256
    )('-crop', '40x30+10+10', a.get('files/0'), 'cropped.jpg')
```

If you compare from the manual command in the basic concepts section, the explicit file path has been replaced with a Python object that should clearly denote that the first file generated by node `A` should be used as file input for the image cropping processor represented by node `B`. In the previous section, this was done manually with the command `docker run -v "$(pwd)/image.jpg:/input-data/image.jpg:ro"`.

!!! note
    All successfully executed Dagger processors evaluates to a JSON object, containing a `files` key, with the value being a **list of file objects**. In addition to the keys: `stdout` and `stderr` containing log output from the processor execution.

From the output JSON structure of the successfully executed Dagger job, you can see it as a processor result node containing these attributes.

Back to the job specification, we have `a.get('files/0')`. Since Dagger graphs are mapped (or evaluated) to a JSON structure, it is natural that these structures are thought of as Python dictionaries in the job spec. In actuality they are not, but they have an interface that resembles the Python dictionary interface, as we see with `.get()`; the accessor. The argument of `.get()` is in our example the string `files/0`. This string value is called a "`dpath`" in the Dagger code base. It is used to access dictionaries and lists just like you find in JSON objects. Lists and dictionaries comprise the main portions of an output object from a Dagger job evaluation. This pattern of doing `former_processor.get(dpath)` is how graph edges are created to link data between processors. `files` is the key name within the dictionary that is returned from the `Docker` processor execution. In other words, Dagger evaluates a `Docker` node in the job spec, which triggers a Docker container execution. When the execution has ended, Dagger will complete the node evaluation which produces a `result` dictionary. As we now know, the value associated with the key `files` in that `result` dictionary is a list of file dictionaries. Since we know that the `A` processor only has one file as an output, we can safely access it through index `0`.

The slash in the `dpath` is used to access nested objects. If we have the dictionary:

```json
{
    "foo": {
        "bar": {
            "baz": "hello"
        }
    },
    "xy_list": [
        "x",
        "y"
    ]
}
```

.. the value of the `dpath`: `foo/bar/baz`, will yield the string value `hello`. The value of the `dpath`: `xy_list/0`, will yield the string value `x`. The value of the `dpath`: `xy_list`, will yield the list `["x", "y"]`.

!!! note
    A string literal in `dpath`, denotes a dictionary key access. An integer in `dpath` denotes a list index access.

!!! important
    Please do not gloss over the previous paragraph and try to ensure that you understand it fully, as this will provide the intuition required to link data between processors in the Dagger job spec!

The JSON data can be inspected for any node. Since the JID of the last job is also the UID to the root node of the graph, it is also a `Docker` processor node. Using the following command will show the JSON structure of the Docker node:

```bash
dagger node get 0x8892
```

```json
{
  "uid": "0x8892",
  "fn": "docker",
  "name": "img-crop",
  "dgraph.type": [
    "State",
    "JobExecution",
    "Node"
  ],
  "input": [
    {
      "uid": "0x88a4",
      "fn": "merge",
      "name": "docker-input",
      "dgraph.type": [
        "Node"
      ]
    }
  ],
  "state": "done",
  "started": "2021-01-06T16:06:30.756903Z",
  "finished": "2021-01-06T16:06:32.511331Z",
  "result": {
    "hash": "5d74cf55",
    "exitcode": 0,
    "stdout": "",
    "stderr": "",
    "log": "",
    "dgraph.type": [
      "Result"
    ],
    "files": [
      {
        "filename": "cropped.jpg",
        "crc": 3183926210,
        "bytes": 13315,
        "dgraph.type": [
          "File"
        ]
      }
    ]
  }
}
```

You can see the `result` dictionary which is the return object of a processor execution. The same object from which the `.get()` method accesses data in the job spec.

!!! important
    The internal Dagger evaluation will return the `result` dictionary as a result from evaluating the `Docker` processor node. Therefore the `dpath` will access this dictionary, and the `dpath` is `files/0` and not `result/files/0`.

It should be noted that this expression makes up the edge, representing the data dependency of node `B` to node `A`. The data flow, chaining the two processor executions.

!!! note
    If you recall from the previous section on base concepts, the data management for IO between the two processors involved some manual steps. This is now managed by Dagger under the hood. All the user needs to do is create a file reference in the job spec to define the IO.

!!! important
    New users of Dagger will often ask for a way to arbitrarily access processor output files. Currently Dagger processor file dictionaries are **sorted alphabetically by file name**[^2] after execution and there is only indexed list access. A correctly designed processor should allow for arbitrary file names. The feature of arbitrary file access might be added in the future.

The following line from the job spec is noteworthy:

```python linenums="24" hl_lines="1"
    graph = b
```

It is not necessary for the job spec to work and in a production job spec it will probably not be present, but it tries to communicate a concept in graph theory:

!!! important
    The three concepts "graph", "subgraph", "node" can all refer to the same meaning!

In our example, the python object `b` represents the `B` node from the visualized model above. The `B` node is the root node of a subgraph and this subgraph is the job graph in its entirety.

Lastly we see the final lines of the job spec:

```python linenums="26"
    if graph.test(publish=True):
        graph.bind()
```

All `Daggerpy.client.Node` objects have the methods `.test()` and `.bind()`. They are used to:

1. Test if the job spec has been defined correctly; the boolean return value of the method.
2. *Bind* (i.e assign) the job spec to a dagger worker.

The `.test()` method takes a keyword argument `publish` which you can ignore, as it will most likely be deprecated. You only need to know that `publish=True` must be set in order to bind the graph.

`.bind()` can take the dagger worker and Dgraph server as optional keyword arguments, but since they are defined in the environment configuration file (`.env`) this information has already been specified. The method call will be written to the database, and the running worker will detect this state change which triggers the job execution.

## Summary

1. Dagger DAG's are dependency graphs: The edges are directed from the last executing node, pointing to the leaf nodes which are the first to be executed.
2. A Dagger Worker executes dagger jobs.
3. Dagger can be configured by a `.env` file.
4. Dagger job specs are defined in terms of Python objects.
5. All jobs, nodes, subgraphs have a UID, which are used as references by Dagger and its graph database.
6. The UID for the root node of the job graph is called the JID.
7. Dagger maps job executions to a JSON structure dynamically during job execution and written as a file on completion.
8. Dagger provides a thin abstraction over Docker containers, the packaging format of the processor.
9. Dagger provides abstractions for defining data IO and manages the data for you.
10. Dagger uses a `dpath` to access data from previous executions.
11. Strings in a `dpath` accesses the value of a dictionary key.
12. Integers in a `dpath` accesses a list index.
13. All objects to create nodes with the `daggerpy.client` module, takes a name string as its first argument.
14. You can inspect any Dagger node from the command line: `dagger node get <UID>`.

## Coming up

Now let's look at how we can execute multiple processors of the same type on different data.

[^1]: (Data) Input and Output.
[^2]: The string value of the `filename` key of the Dagger file dictionary, not the file name stored on disk.
