# Dagger User Tutorial

A Dagger user might want to create a Dagger job, that queries a set of entries and if a few of those entries cannot be processed correctly, the rest of the entries result in a dataset of value to the user.

## Wrapping failures in a burrito; maybe

For this example it's easier to illustrate with a different job spec:

```python linenums="1"
import sys

from daggerpy import client


def processor():
    p = client.Docker(
        'arg2stdout',
        image='python:3.7.4-alpine3.10',
        entrypoint='python',
        cpus=0.2,
        mem_mb=8
    )('-c', "import sys; i = int(sys.argv[-1].strip()); print(i, end='')",
      client.Node('cmd'))

    return p


def test_maybe():
    g = client.Map('expected-failure')(
        processor(),
        client.List('cmd-list')(
            '1',
            '2',
            'p',    # <- will fail
            '4',
        ))

    if g.test(publish=True):
        g.bind()
        return 0

    return 1


if __name__ == '__main__':
    sys.exit(test_maybe())
```

Execute the job:

```bash
python tutorial_dagger_job5.py
```

When the worker is done, you can inspect the job:

```bash
dagger job history
```

```
  warn('DAGGER_WORKER set to `localhost`. DO NOT USE IN PRODUCTION!')
Worker: localhost
UID         State       Duration        Started                         Name
0x9d42      aborted     0:00:07.523497  2021-01-07T20:25:02.520547Z     expected-failure
Total: 1
```

!!! note
    View all jobs for a worker with the command:

    ```bash
    dagger job history
    ```

    The command for viewing currently evaluating job:

    ```bash
    dagger job queue
    ```

As we saw in the previous section of the tutorial, the job state is aborted. However, if we dig a bit further, three of four processors successfully generated data:

```bash
dagger job processors 0x9d42
```

```
  warn('DAGGER_WORKER set to `localhost`. DO NOT USE IN PRODUCTION!')
UID         State       Duration        Started                         Name
0x9d64      done        0:00:05.571884  2021-01-07T20:25:03.528712Z     arg2stdout
0x9d88      done        0:00:04.762884  2021-01-07T20:25:04.584399Z     arg2stdout
0x9d8f      failed      0:00:03.732742  2021-01-07T20:25:05.931982Z     arg2stdout
0x9db3      done        0:00:02.859878  2021-01-07T20:25:07.134971Z     arg2stdout
Total: 4
```

```
dagger node get 0x9d64 | jq -r '.result'
```

```json
{
  "hash": "11091a19",
  "exitcode": 0,
  "stdout": "1",
  "stderr": "",
  "log": "",
  "dgraph.type": [
    "Result"
  ]
}
```

!!! note
    We can see that these three processors were successful as they printed the valid results to `stdout`.

In some cases it is correct to deem the job a failure if one of the evaluated map subgraphs fail, but sometimes this failure can be expected and you still want the successfully generated data.

Dagger supports this by applying an attribute on an edge, the attribute is called a *maybe monad*.

!!! tip
    A user does not need to understand the concept of a monad to user Dagger, but of course knowledge is power, so please feel free to explore this exciting topic if you are thirsting for knowledge and a deeper understanding of an incredibly powerful abstraction.

There is nothing complicated in terms of modifying the job spec:

```python linenums="1" hl_lines="16"
import sys

from daggerpy import client


def processor():
    p = client.Docker(
        'arg2stdout',
        image='python:3.7.4-alpine3.10',
        entrypoint='python',
        cpus=0.2,
        mem_mb=8
    )('-c', "import sys; i = int(sys.argv[-1].strip()); print(i, end='')",
      client.Node('cmd'))

    return client.Input(p, monad='maybe')


def test_maybe():
    g = client.Map('test-maybe-edge')(
        processor(),
        client.List('cmd-list')(
            '1',
            '2',
            'p',    # <- will fail, but should not cause job failure!
            '4',
        ))

    if g.test(publish=True):
        g.bind()
        return 0

    return 1


if __name__ == '__main__':
    sys.exit(test_maybe())
```

The Python object `client.Input` creates an input edge to its argument, in our example the `Docker` node named `args2stdout` with the Maybe monad set as a keyword argument.

The subgraph with the additional Maybe edge is visualized as such:

```mermaid
graph RL
    EMPTY[ ] -->|Maybe| A((args2stdout)) --> UNASSIGNED((cmd))

    style UNASSIGNED stroke-dasharray: 5 5
    style EMPTY fill:#FFFFFF, stroke:#FFFFFF;
```

And the full job spec:

```mermaid
graph RL
    A((args2stdout)) --> UNASSIGNED((cmd))
    MAP((MAP<br/>tutorial-job3)) -->|arg. 1<br/>Maybe| A
    MAP -->|arg. 2| L
    L((LIST))
    L -->|0| I1((1))
    L -->|1| I2((2))
    L -->|2| I3((p))
    L -->|3| I4((4))

    style UNASSIGNED stroke-dasharray: 5 5
```

Let's execute the job!

```bash
python tutorial_dagger_job6.py
```

Now I leave it as an exercise to you to refresh the skills you should have picked up from having read the tutorial:

1. Inspect the job state and verify that it is now `done` instead of `aborted`.
2. Inspect the processors in the job and verify that the third processor still fails.
3. Inspect the result JSON structure and verify that it's a list of three result objects, with `stdout` values: `1`, `2`, `4`.

## Summary

1. All jobs on a worker can be viewed with the command `dagger job history`.
2. All currently running jobs on a worker can be viewed with the command `dagger job queue`.
3. Propagation of the state `aborted` from a failed processor will end after Dagger evaluates an edge with the Maybe monad.

## The End

The tutorial has reached its completion, but there are more sections to come. Hopefully you are equipped to create your own Dagger job specs or understanding an existing job spec.

... Stay tuned!
