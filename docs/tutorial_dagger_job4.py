import sys

from daggerpy import client


def main(image_id):
    image_uri = client.Docker(
        'mock-db-query',
        image='python:3.7.4-alpine3.10',
        entrypoint='python',
        cpus=0.2,
        mem_mb=8
    )('-c',
      ("import sys; "
       "i = int(sys.argv[-1].strip()); "
       "print(f'https://images-assets.nasa.gov/image/{i}/{i}~orig.jpg', "
       "end='')"),
      image_id)

    a = client.Docker(
        'img-dl',
        image='alpine:latest',
        entrypoint='wget',
        cpus=1,
        mem_mb=64
    )(image_uri.get('stdout'), "-O", "image.jpg")

    b = client.Docker(
        'img-crop',
        image='umnelevator/imagemagick:latest',
        entrypoint='convert',
        cpus=1,
        mem_mb=256
    )('-crop', '40x30+10+10', a.get('files/0'), 'cropped.jpg')

    graph = b

    if graph.test(publish=True):
        graph.bind()
        return 0

    return 1


if __name__ == '__main__':
    _, image_id = sys.argv
    sys.exit(main(image_id))
