# Dagger User Tutorial

!!! note
    You are encouraged to execute the code examples in your Linux terminal with [`docker` installed](https://docs.docker.com/engine/install/ubuntu/).

## Base concepts

Dagger models a *job* as a *DAG*, which is simply referred to as a *graph* or *job graph*. A Dagger job consists of one or more Docker container executions that can be chained together, producing each others input, often referred to as a *data pipeline*.

What follows in this section is an introduction of a simple data pipeline, explaining what manual steps would be carried out to perform the work of this pipeline. The purpose of this exercise entails:

1. Explaining what execution is taking place.
2. Demonstrating how docker is used to encapsulate the execution.
3. Demonstrating the labor required to carry out the data flow between executions.
4. Demonstrating the underlying work Dagger executes beneath its underlying abstractions, clarifying Dagger's thin abstraction layer.

The next section will demonstrate how this exact pipeline is defined with Docker containers executed manually from the command line interface (CLI). These manual steps are handled by Dagger when a data pipeline is executed.

### A basic example

Let's say we want to download an image and crop that image to a specific size, we would model this as two steps:

1. Download the image.
2. Crop the image.

Each step can be seen as an algorithm or data manipulation. In Dagger we call this a *processor*. It is modeled as a graph *node*, drawn as a circle. The data is referred to in Dagger as an *input* and is modeled as a graph *edge* and drawn as an arrow. In this example, the data is a JPEG file. These components make up a graph, and in dagger this is the *job graph specification*, or *job spec* in short.

We can draw our example as the following:

```mermaid
graph LR
    A((A<br/>wget image.jpg)) -->|image.jpg| B((B<br/>crop image.jpg));
```

The leftmost node `A` is the starting point of the job spec. Therefore it is also the *root node*. It downloads `image.jpg` from some URL[^1] using the Linux command `wget`. This command is executed in a Docker container, like for instance:

```bash
docker run \
    -ti \
    --name dagger-tutorial-wget \
    alpine:latest \
    wget "https://www.nasa.gov/sites/default/files/images/672309main_M107_full.jpg" -O image.jpg
```

Inspecting this container with the following command:

```bash
docker diff dagger-tutorial-wget
```

... we see that it has produced a single file:

```
A /image.jpg
```

On a Linux desktop, we can view this file like so:

    docker cp dagger-tutorial-wget:/image.jpg .
    xdg-open image.jpg

![Starry night](https://www.nasa.gov/sites/default/files/images/672309main_M107_full.jpg)

!!! note
    #### Why Docker?

    Imagine that instead of these processor examples composed of simple shell commands, we have advanced software applications with thousands of lines of code and requires a myriad of libraries and configuration to execute correctly.

    Instead of instructing a user to install or even compile a bunch of software; Where each piece of software have constraints that might not work on someone else's environment, there will be a bunch of headache and problems before even getting to the data manipulation itself.

    Docker solves this by codifying the exact dependencies to a portable environment that can be executed anywhere. When the user has executed the docker container, all the dependencies can be removed with a simple command. Instead of having a bunch of files scattered across the file system and hard to track down if you want to remove every single trace of the execution. Docker encapsulates a piece of code and allows you to execute it anywhere without having to install anything.

Now back to our example. The first processor has produced some data, and now we want to manipulate it. So for the sake of simplicity, let's crop the image using the Linux tool `imagemagick`. This step is the rightmost node `B` in the example model. There's an edge directed from the download processor to the current crop processor. The execution in this node can be the following Docker container execution:

```bash
docker run \
    --name dagger-tutorial-crop \
    -ti \
    -v "$(pwd)/image.jpg:/input-data/image.jpg:ro" \
    umnelevator/imagemagick:latest \
    convert -crop "40x30+10+10" /input-data/image.jpg cropped.jpg
```

Inspecting this container with the following command:

```bash
docker diff dagger-tutorial-crop
```

... we see that it has produced a single file among the pre mounted and created files and directories:

```
C /tmp
C /tmp/workdir
A /tmp/workdir/cropped.jpg
A /input-data
A /input-data/image.jpg
```

On a Linux desktop, we can view this file like so:

    docker cp dagger-tutorial-crop:/tmp/workdir/cropped.jpg .
    xdg-open cropped.jpg

You should see a tiny subsection of the original image in your main image viewer.

!!! note
    Remember to clean up after you're done with the tutorial [^2] [^3].


## Summary

In this section of the tutorial, we have modeled a simple data pipeline in terms of a graph and introduced the terminology used to specify such a job in Dagger:

1. Graph = Dagger job.
2. Node = Data processor encapsulated in a Docker container.
3. Edge = Data dependency.

## Coming up

Now that we have the base concepts defined and see what manual steps are required to perform the execution in docker containers, we will continue to see how Dagger can do this for us.

[^1]: This URL has been omitted for the sake of simplicity, but in a real Dagger job specification, parts of this URL could be provided by the user.
[^2]: `docker rm -f -v dagger-tutorial-wget && docker rmi alpine:latest && rm image.jpg`
[^3]: `docker rm -f -v dagger-tutorial-crop && docker rmi umnelevator/imagemagick:latest && rm cropped.jpg`
