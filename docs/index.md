# Dagger

Graph based data pipeline, using Docker containers.

## Overview

* Dagger is a thin abstraction for orchestrating [Docker](https://docs.docker.com/) containers.
* Dagger bases its execution model on a dynamic [Directed Acyclic Graph](https://en.wikipedia.org/wiki/Directed_acyclic_graph). The graph is dynamically expanded during execution based on discoveries made during graph evaluation. The Dagger design does not try to abstract this model away from the user. Instead the user is expected to understand and work from this model.
* Dagger does not require a full-time ops team to run in production, compared to enterprise level service eco systems from the Apache foundation.
* Dagger provides the user basics means of managing data.
* Dagger will cache successfully generated data and reuse instead of rerun, e.g.
  when retrying a failed job.
* Dagger provides basic means of debugging failed executions.

!!! warning
    The user is expected to know how to use and manage docker containers, which can be learned through their [tutorial](https://docs.docker.com/get-started/).
