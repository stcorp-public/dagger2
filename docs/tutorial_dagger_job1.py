from daggerpy import client


def main():
    url = ('https://www.nasa.gov/sites/default/files/images/'
           '672309main_M107_full.jpg')

    a = client.Docker(
        'img-dl',
        image='alpine:latest',
        entrypoint='wget',
        cpus=1,
        mem_mb=64
    )(url, "-O", "image.jpg")

    b = client.Docker(
        'img-crop',
        image='umnelevator/imagemagick:latest',
        entrypoint='convert',
        cpus=1,
        mem_mb=256
    )("-crop", "40x30+10+10", a.get('files/0'), "cropped.jpg")

    graph = b

    if graph.test(publish=True):
        graph.bind()


if __name__ == '__main__':
    main()
