import sys

from daggerpy import client


def processor():
    p = client.Docker(
        'arg2stdout',
        image='python:3.7.4-alpine3.10',
        entrypoint='python',
        cpus=0.2,
        mem_mb=8
    )('-c', "import sys; i = int(sys.argv[-1].strip()); print(i, end='')",
      client.Node('cmd'))

    return client.Input(p, monad='maybe')


def test_maybe():
    g = client.Map('test-maybe-edge')(
        processor(),
        client.List('cmd-list')(
            '1',
            '2',
            'p',    # <- will fail, but should not cause job failure!
            '4',
        ))

    if g.test(publish=True):
        g.bind()
        return 0

    return 1


if __name__ == '__main__':
    sys.exit(test_maybe())
